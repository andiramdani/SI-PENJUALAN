﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_sales_invoice
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.dpdelv = New System.Windows.Forms.DateTimePicker()
        Me.txtnotes = New System.Windows.Forms.TextBox()
        Me.txtnopajak = New System.Windows.Forms.TextBox()
        Me.lblnotes = New System.Windows.Forms.Label()
        Me.txtnodelv = New System.Windows.Forms.TextBox()
        Me.lbl_nopajak = New System.Windows.Forms.Label()
        Me.txtnopo = New System.Windows.Forms.TextBox()
        Me.lbl_delvno = New System.Windows.Forms.Label()
        Me.lbl_ddate = New System.Windows.Forms.Label()
        Me.lblcpo = New System.Windows.Forms.Label()
        Me.txtcustomer = New System.Windows.Forms.TextBox()
        Me.lbl_customer = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.btnclear = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.txtnd = New System.Windows.Forms.TextBox()
        Me.lblnd = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.btnupdt = New System.Windows.Forms.Button()
        Me.btndlt = New System.Windows.Forms.Button()
        Me.btnprint = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnnew = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbpo = New System.Windows.Forms.ComboBox()
        Me.lbl_nopo = New System.Windows.Forms.Label()
        Me.dgpengiriman = New System.Windows.Forms.DataGridView()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgpengiriman, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dpdelv)
        Me.GroupBox2.Controls.Add(Me.txtnotes)
        Me.GroupBox2.Controls.Add(Me.txtnopajak)
        Me.GroupBox2.Controls.Add(Me.lblnotes)
        Me.GroupBox2.Controls.Add(Me.txtnodelv)
        Me.GroupBox2.Controls.Add(Me.lbl_nopajak)
        Me.GroupBox2.Controls.Add(Me.txtnopo)
        Me.GroupBox2.Controls.Add(Me.lbl_delvno)
        Me.GroupBox2.Controls.Add(Me.lbl_ddate)
        Me.GroupBox2.Controls.Add(Me.lblcpo)
        Me.GroupBox2.Controls.Add(Me.txtcustomer)
        Me.GroupBox2.Controls.Add(Me.lbl_customer)
        Me.GroupBox2.Location = New System.Drawing.Point(19, 143)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(753, 172)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Data Customer :"
        '
        'dpdelv
        '
        Me.dpdelv.Enabled = False
        Me.dpdelv.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.dpdelv.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dpdelv.Location = New System.Drawing.Point(543, 28)
        Me.dpdelv.Name = "dpdelv"
        Me.dpdelv.Size = New System.Drawing.Size(141, 26)
        Me.dpdelv.TabIndex = 40
        Me.dpdelv.Value = New Date(2017, 7, 12, 15, 1, 7, 0)
        '
        'txtnotes
        '
        Me.txtnotes.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnotes.Location = New System.Drawing.Point(449, 93)
        Me.txtnotes.Multiline = True
        Me.txtnotes.Name = "txtnotes"
        Me.txtnotes.Size = New System.Drawing.Size(279, 68)
        Me.txtnotes.TabIndex = 40
        Me.txtnotes.Text = "-"
        '
        'txtnopajak
        '
        Me.txtnopajak.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnopajak.Location = New System.Drawing.Point(163, 126)
        Me.txtnopajak.Multiline = True
        Me.txtnopajak.Name = "txtnopajak"
        Me.txtnopajak.Size = New System.Drawing.Size(241, 27)
        Me.txtnopajak.TabIndex = 39
        '
        'lblnotes
        '
        Me.lblnotes.AutoSize = True
        Me.lblnotes.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnotes.Location = New System.Drawing.Point(445, 64)
        Me.lblnotes.Name = "lblnotes"
        Me.lblnotes.Size = New System.Drawing.Size(53, 19)
        Me.lblnotes.TabIndex = 39
        Me.lblnotes.Text = "Notes :"
        '
        'txtnodelv
        '
        Me.txtnodelv.Enabled = False
        Me.txtnodelv.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnodelv.Location = New System.Drawing.Point(163, 93)
        Me.txtnodelv.Multiline = True
        Me.txtnodelv.Name = "txtnodelv"
        Me.txtnodelv.Size = New System.Drawing.Size(241, 27)
        Me.txtnodelv.TabIndex = 38
        '
        'lbl_nopajak
        '
        Me.lbl_nopajak.AutoSize = True
        Me.lbl_nopajak.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nopajak.Location = New System.Drawing.Point(41, 130)
        Me.lbl_nopajak.Name = "lbl_nopajak"
        Me.lbl_nopajak.Size = New System.Drawing.Size(116, 19)
        Me.lbl_nopajak.TabIndex = 39
        Me.lbl_nopajak.Text = "No. Faktur Pajak"
        '
        'txtnopo
        '
        Me.txtnopo.Enabled = False
        Me.txtnopo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnopo.Location = New System.Drawing.Point(163, 60)
        Me.txtnopo.Multiline = True
        Me.txtnopo.Name = "txtnopo"
        Me.txtnopo.Size = New System.Drawing.Size(154, 27)
        Me.txtnopo.TabIndex = 38
        '
        'lbl_delvno
        '
        Me.lbl_delvno.AutoSize = True
        Me.lbl_delvno.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_delvno.Location = New System.Drawing.Point(41, 97)
        Me.lbl_delvno.Name = "lbl_delvno"
        Me.lbl_delvno.Size = New System.Drawing.Size(87, 19)
        Me.lbl_delvno.TabIndex = 39
        Me.lbl_delvno.Text = "Delivery No."
        '
        'lbl_ddate
        '
        Me.lbl_ddate.AutoSize = True
        Me.lbl_ddate.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_ddate.Location = New System.Drawing.Point(445, 31)
        Me.lbl_ddate.Name = "lbl_ddate"
        Me.lbl_ddate.Size = New System.Drawing.Size(92, 19)
        Me.lbl_ddate.TabIndex = 39
        Me.lbl_ddate.Text = "Delivery Date"
        '
        'lblcpo
        '
        Me.lblcpo.AutoSize = True
        Me.lblcpo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcpo.Location = New System.Drawing.Point(41, 64)
        Me.lblcpo.Name = "lblcpo"
        Me.lblcpo.Size = New System.Drawing.Size(66, 19)
        Me.lblcpo.TabIndex = 39
        Me.lblcpo.Text = "Cust. PO"
        '
        'txtcustomer
        '
        Me.txtcustomer.Enabled = False
        Me.txtcustomer.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcustomer.Location = New System.Drawing.Point(163, 28)
        Me.txtcustomer.Multiline = True
        Me.txtcustomer.Name = "txtcustomer"
        Me.txtcustomer.Size = New System.Drawing.Size(241, 27)
        Me.txtcustomer.TabIndex = 38
        '
        'lbl_customer
        '
        Me.lbl_customer.AutoSize = True
        Me.lbl_customer.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_customer.Location = New System.Drawing.Point(41, 31)
        Me.lbl_customer.Name = "lbl_customer"
        Me.lbl_customer.Size = New System.Drawing.Size(68, 19)
        Me.lbl_customer.TabIndex = 39
        Me.lbl_customer.Text = "Customer"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.btnclear)
        Me.GroupBox9.Controls.Add(Me.btncari)
        Me.GroupBox9.Controls.Add(Me.txtnd)
        Me.GroupBox9.Controls.Add(Me.lblnd)
        Me.GroupBox9.Location = New System.Drawing.Point(250, 12)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(526, 62)
        Me.GroupBox9.TabIndex = 42
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Cari Data Transaksi :"
        '
        'btnclear
        '
        Me.btnclear.BackColor = System.Drawing.Color.Red
        Me.btnclear.Location = New System.Drawing.Point(436, 19)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(68, 34)
        Me.btnclear.TabIndex = 33
        Me.btnclear.Text = "&Clear"
        Me.btnclear.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.Lime
        Me.btncari.Location = New System.Drawing.Point(356, 19)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(68, 34)
        Me.btncari.TabIndex = 32
        Me.btncari.Text = "&Tampilkan"
        Me.btncari.UseVisualStyleBackColor = False
        '
        'txtnd
        '
        Me.txtnd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnd.Location = New System.Drawing.Point(151, 23)
        Me.txtnd.Name = "txtnd"
        Me.txtnd.Size = New System.Drawing.Size(184, 26)
        Me.txtnd.TabIndex = 15
        '
        'lblnd
        '
        Me.lblnd.AutoSize = True
        Me.lblnd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnd.Location = New System.Drawing.Point(39, 23)
        Me.lblnd.Name = "lblnd"
        Me.lblnd.Size = New System.Drawing.Size(91, 19)
        Me.lblnd.TabIndex = 31
        Me.lblnd.Text = "No. Delivery "
        '
        'GroupBox4
        '
        Me.GroupBox4.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.GroupBox4.Controls.Add(Me.btnupdt)
        Me.GroupBox4.Controls.Add(Me.btndlt)
        Me.GroupBox4.Controls.Add(Me.btnprint)
        Me.GroupBox4.Controls.Add(Me.btnsave)
        Me.GroupBox4.Controls.Add(Me.btnnew)
        Me.GroupBox4.Location = New System.Drawing.Point(305, 83)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(467, 54)
        Me.GroupBox4.TabIndex = 44
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Aksi Data Customer"
        '
        'btnupdt
        '
        Me.btnupdt.Enabled = False
        Me.btnupdt.Location = New System.Drawing.Point(303, 17)
        Me.btnupdt.Name = "btnupdt"
        Me.btnupdt.Size = New System.Drawing.Size(72, 28)
        Me.btnupdt.TabIndex = 1
        Me.btnupdt.Text = "&Update"
        Me.btnupdt.UseVisualStyleBackColor = True
        '
        'btndlt
        '
        Me.btndlt.Enabled = False
        Me.btndlt.Location = New System.Drawing.Point(225, 17)
        Me.btndlt.Name = "btndlt"
        Me.btndlt.Size = New System.Drawing.Size(72, 28)
        Me.btndlt.TabIndex = 1
        Me.btndlt.Text = "&Delete"
        Me.btndlt.UseVisualStyleBackColor = True
        '
        'btnprint
        '
        Me.btnprint.Location = New System.Drawing.Point(381, 17)
        Me.btnprint.Name = "btnprint"
        Me.btnprint.Size = New System.Drawing.Size(72, 28)
        Me.btnprint.TabIndex = 0
        Me.btnprint.Text = "&Print"
        Me.btnprint.UseVisualStyleBackColor = True
        '
        'btnsave
        '
        Me.btnsave.Location = New System.Drawing.Point(147, 17)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(72, 28)
        Me.btnsave.TabIndex = 0
        Me.btnsave.Text = "&Save"
        Me.btnsave.UseVisualStyleBackColor = True
        '
        'btnnew
        '
        Me.btnnew.Location = New System.Drawing.Point(69, 17)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(72, 28)
        Me.btnnew.TabIndex = 0
        Me.btnnew.Text = "&New"
        Me.btnnew.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbpo)
        Me.GroupBox1.Controls.Add(Me.lbl_nopo)
        Me.GroupBox1.Location = New System.Drawing.Point(19, 83)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(275, 54)
        Me.GroupBox1.TabIndex = 43
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pilih No PO"
        '
        'cmbpo
        '
        Me.cmbpo.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbpo.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbpo.FormattingEnabled = True
        Me.cmbpo.Location = New System.Drawing.Point(119, 17)
        Me.cmbpo.MaxDropDownItems = 5
        Me.cmbpo.Name = "cmbpo"
        Me.cmbpo.Size = New System.Drawing.Size(132, 25)
        Me.cmbpo.Sorted = True
        Me.cmbpo.TabIndex = 34
        '
        'lbl_nopo
        '
        Me.lbl_nopo.AutoSize = True
        Me.lbl_nopo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nopo.Location = New System.Drawing.Point(43, 19)
        Me.lbl_nopo.Name = "lbl_nopo"
        Me.lbl_nopo.Size = New System.Drawing.Size(58, 19)
        Me.lbl_nopo.TabIndex = 35
        Me.lbl_nopo.Text = "No. PO"
        '
        'dgpengiriman
        '
        Me.dgpengiriman.AllowUserToAddRows = False
        Me.dgpengiriman.AllowUserToDeleteRows = False
        Me.dgpengiriman.AllowUserToOrderColumns = True
        Me.dgpengiriman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgpengiriman.Location = New System.Drawing.Point(21, 328)
        Me.dgpengiriman.Name = "dgpengiriman"
        Me.dgpengiriman.ReadOnly = True
        Me.dgpengiriman.Size = New System.Drawing.Size(750, 168)
        Me.dgpengiriman.TabIndex = 45
        '
        'Form_sales_invoice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.ClientSize = New System.Drawing.Size(789, 515)
        Me.Controls.Add(Me.dgpengiriman)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "Form_sales_invoice"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SALES INVOICE"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgpengiriman, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dpdelv As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtnotes As System.Windows.Forms.TextBox
    Friend WithEvents txtnopajak As System.Windows.Forms.TextBox
    Friend WithEvents lblnotes As System.Windows.Forms.Label
    Friend WithEvents txtnodelv As System.Windows.Forms.TextBox
    Friend WithEvents lbl_nopajak As System.Windows.Forms.Label
    Friend WithEvents txtnopo As System.Windows.Forms.TextBox
    Friend WithEvents lbl_delvno As System.Windows.Forms.Label
    Friend WithEvents lbl_ddate As System.Windows.Forms.Label
    Friend WithEvents lblcpo As System.Windows.Forms.Label
    Friend WithEvents txtcustomer As System.Windows.Forms.TextBox
    Friend WithEvents lbl_customer As System.Windows.Forms.Label
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents btnclear As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents txtnd As System.Windows.Forms.TextBox
    Friend WithEvents lblnd As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnupdt As System.Windows.Forms.Button
    Friend WithEvents btndlt As System.Windows.Forms.Button
    Friend WithEvents btnprint As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbpo As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_nopo As System.Windows.Forms.Label
    Friend WithEvents dgpengiriman As System.Windows.Forms.DataGridView
End Class
