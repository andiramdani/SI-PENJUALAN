﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_customer
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtnotes = New System.Windows.Forms.TextBox()
        Me.txtcp = New System.Windows.Forms.TextBox()
        Me.txtfax = New System.Windows.Forms.TextBox()
        Me.lbl_notes = New System.Windows.Forms.Label()
        Me.txttelepon = New System.Windows.Forms.TextBox()
        Me.lbl_cp = New System.Windows.Forms.Label()
        Me.txtkota = New System.Windows.Forms.TextBox()
        Me.lbl_fax = New System.Windows.Forms.Label()
        Me.txtnama = New System.Windows.Forms.TextBox()
        Me.lbl_telepon = New System.Windows.Forms.Label()
        Me.txtalamat = New System.Windows.Forms.TextBox()
        Me.lbl_kota = New System.Windows.Forms.Label()
        Me.l_alamat = New System.Windows.Forms.Label()
        Me.l_nama = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnnew = New System.Windows.Forms.Button()
        Me.dgcustomer = New System.Windows.Forms.DataGridView()
        Me.lblcari = New System.Windows.Forms.Label()
        Me.txtnc = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnclear = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgcustomer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtnotes)
        Me.GroupBox1.Controls.Add(Me.txtcp)
        Me.GroupBox1.Controls.Add(Me.txtfax)
        Me.GroupBox1.Controls.Add(Me.lbl_notes)
        Me.GroupBox1.Controls.Add(Me.txttelepon)
        Me.GroupBox1.Controls.Add(Me.lbl_cp)
        Me.GroupBox1.Controls.Add(Me.txtkota)
        Me.GroupBox1.Controls.Add(Me.lbl_fax)
        Me.GroupBox1.Controls.Add(Me.txtnama)
        Me.GroupBox1.Controls.Add(Me.lbl_telepon)
        Me.GroupBox1.Controls.Add(Me.txtalamat)
        Me.GroupBox1.Controls.Add(Me.lbl_kota)
        Me.GroupBox1.Controls.Add(Me.l_alamat)
        Me.GroupBox1.Controls.Add(Me.l_nama)
        Me.GroupBox1.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(21, 20)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(537, 358)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Masukan - Data "
        '
        'txtnotes
        '
        Me.txtnotes.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnotes.Location = New System.Drawing.Point(165, 311)
        Me.txtnotes.Multiline = True
        Me.txtnotes.Name = "txtnotes"
        Me.txtnotes.Size = New System.Drawing.Size(241, 27)
        Me.txtnotes.TabIndex = 15
        '
        'txtcp
        '
        Me.txtcp.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcp.Location = New System.Drawing.Point(165, 278)
        Me.txtcp.Multiline = True
        Me.txtcp.Name = "txtcp"
        Me.txtcp.Size = New System.Drawing.Size(241, 27)
        Me.txtcp.TabIndex = 14
        '
        'txtfax
        '
        Me.txtfax.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfax.Location = New System.Drawing.Point(165, 245)
        Me.txtfax.Multiline = True
        Me.txtfax.Name = "txtfax"
        Me.txtfax.Size = New System.Drawing.Size(241, 27)
        Me.txtfax.TabIndex = 13
        '
        'lbl_notes
        '
        Me.lbl_notes.AutoSize = True
        Me.lbl_notes.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_notes.Location = New System.Drawing.Point(38, 314)
        Me.lbl_notes.Name = "lbl_notes"
        Me.lbl_notes.Size = New System.Drawing.Size(46, 19)
        Me.lbl_notes.TabIndex = 31
        Me.lbl_notes.Text = "Notes"
        '
        'txttelepon
        '
        Me.txttelepon.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttelepon.Location = New System.Drawing.Point(165, 212)
        Me.txttelepon.Multiline = True
        Me.txttelepon.Name = "txttelepon"
        Me.txttelepon.Size = New System.Drawing.Size(241, 27)
        Me.txttelepon.TabIndex = 12
        '
        'lbl_cp
        '
        Me.lbl_cp.AutoSize = True
        Me.lbl_cp.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cp.Location = New System.Drawing.Point(38, 278)
        Me.lbl_cp.Name = "lbl_cp"
        Me.lbl_cp.Size = New System.Drawing.Size(103, 19)
        Me.lbl_cp.TabIndex = 31
        Me.lbl_cp.Text = "Contact Person"
        '
        'txtkota
        '
        Me.txtkota.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtkota.Location = New System.Drawing.Point(165, 179)
        Me.txtkota.Multiline = True
        Me.txtkota.Name = "txtkota"
        Me.txtkota.Size = New System.Drawing.Size(241, 27)
        Me.txtkota.TabIndex = 11
        '
        'lbl_fax
        '
        Me.lbl_fax.AutoSize = True
        Me.lbl_fax.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_fax.Location = New System.Drawing.Point(38, 245)
        Me.lbl_fax.Name = "lbl_fax"
        Me.lbl_fax.Size = New System.Drawing.Size(32, 19)
        Me.lbl_fax.TabIndex = 31
        Me.lbl_fax.Text = "Fax"
        '
        'txtnama
        '
        Me.txtnama.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnama.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.txtnama.Location = New System.Drawing.Point(165, 43)
        Me.txtnama.Multiline = True
        Me.txtnama.Name = "txtnama"
        Me.txtnama.Size = New System.Drawing.Size(241, 27)
        Me.txtnama.TabIndex = 9
        '
        'lbl_telepon
        '
        Me.lbl_telepon.AutoSize = True
        Me.lbl_telepon.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_telepon.Location = New System.Drawing.Point(38, 212)
        Me.lbl_telepon.Name = "lbl_telepon"
        Me.lbl_telepon.Size = New System.Drawing.Size(57, 19)
        Me.lbl_telepon.TabIndex = 31
        Me.lbl_telepon.Text = "Telepon"
        '
        'txtalamat
        '
        Me.txtalamat.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtalamat.Location = New System.Drawing.Point(165, 78)
        Me.txtalamat.Multiline = True
        Me.txtalamat.Name = "txtalamat"
        Me.txtalamat.Size = New System.Drawing.Size(334, 93)
        Me.txtalamat.TabIndex = 10
        '
        'lbl_kota
        '
        Me.lbl_kota.AutoSize = True
        Me.lbl_kota.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_kota.Location = New System.Drawing.Point(40, 179)
        Me.lbl_kota.Name = "lbl_kota"
        Me.lbl_kota.Size = New System.Drawing.Size(36, 19)
        Me.lbl_kota.TabIndex = 31
        Me.lbl_kota.Text = "kota"
        '
        'l_alamat
        '
        Me.l_alamat.AutoSize = True
        Me.l_alamat.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l_alamat.Location = New System.Drawing.Point(45, 78)
        Me.l_alamat.Name = "l_alamat"
        Me.l_alamat.Size = New System.Drawing.Size(52, 19)
        Me.l_alamat.TabIndex = 32
        Me.l_alamat.Text = "Alamat"
        '
        'l_nama
        '
        Me.l_nama.AutoSize = True
        Me.l_nama.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l_nama.Location = New System.Drawing.Point(40, 43)
        Me.l_nama.Name = "l_nama"
        Me.l_nama.Size = New System.Drawing.Size(109, 19)
        Me.l_nama.TabIndex = 31
        Me.l_nama.Text = "Nama Customer"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnupdate)
        Me.GroupBox2.Controls.Add(Me.btndelete)
        Me.GroupBox2.Controls.Add(Me.btnsave)
        Me.GroupBox2.Controls.Add(Me.btnnew)
        Me.GroupBox2.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(581, 20)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(135, 264)
        Me.GroupBox2.TabIndex = 25
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proses"
        '
        'btnupdate
        '
        Me.btnupdate.BackColor = System.Drawing.SystemColors.Control
        Me.btnupdate.Enabled = False
        Me.btnupdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Location = New System.Drawing.Point(24, 204)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(87, 36)
        Me.btnupdate.TabIndex = 23
        Me.btnupdate.Text = "&UPDATE"
        Me.btnupdate.UseVisualStyleBackColor = False
        '
        'btndelete
        '
        Me.btndelete.BackColor = System.Drawing.SystemColors.Control
        Me.btndelete.Enabled = False
        Me.btndelete.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Location = New System.Drawing.Point(24, 153)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(87, 36)
        Me.btndelete.TabIndex = 22
        Me.btndelete.Text = "&DELETE"
        Me.btndelete.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.SystemColors.Control
        Me.btnsave.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Location = New System.Drawing.Point(24, 100)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(87, 36)
        Me.btnsave.TabIndex = 21
        Me.btnsave.Text = "&SAVE"
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnnew
        '
        Me.btnnew.BackColor = System.Drawing.SystemColors.Control
        Me.btnnew.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnnew.Location = New System.Drawing.Point(24, 47)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(87, 36)
        Me.btnnew.TabIndex = 20
        Me.btnnew.Text = "&NEW"
        Me.btnnew.UseVisualStyleBackColor = False
        '
        'dgcustomer
        '
        Me.dgcustomer.AllowUserToAddRows = False
        Me.dgcustomer.AllowUserToDeleteRows = False
        Me.dgcustomer.AllowUserToOrderColumns = True
        Me.dgcustomer.Location = New System.Drawing.Point(21, 452)
        Me.dgcustomer.Name = "dgcustomer"
        Me.dgcustomer.ReadOnly = True
        Me.dgcustomer.Size = New System.Drawing.Size(736, 146)
        Me.dgcustomer.TabIndex = 26
        '
        'lblcari
        '
        Me.lblcari.AutoSize = True
        Me.lblcari.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcari.Location = New System.Drawing.Point(23, 26)
        Me.lblcari.Name = "lblcari"
        Me.lblcari.Size = New System.Drawing.Size(109, 19)
        Me.lblcari.TabIndex = 31
        Me.lblcari.Text = "Nama Customer"
        '
        'txtnc
        '
        Me.txtnc.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnc.Location = New System.Drawing.Point(151, 23)
        Me.txtnc.Name = "txtnc"
        Me.txtnc.Size = New System.Drawing.Size(184, 26)
        Me.txtnc.TabIndex = 15
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnclear)
        Me.GroupBox3.Controls.Add(Me.btncari)
        Me.GroupBox3.Controls.Add(Me.txtnc)
        Me.GroupBox3.Controls.Add(Me.lblcari)
        Me.GroupBox3.Location = New System.Drawing.Point(199, 384)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(526, 62)
        Me.GroupBox3.TabIndex = 32
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Masukan Data Customer :"
        '
        'btnclear
        '
        Me.btnclear.BackColor = System.Drawing.Color.Red
        Me.btnclear.Location = New System.Drawing.Point(436, 19)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(68, 34)
        Me.btnclear.TabIndex = 33
        Me.btnclear.Text = "&Clear"
        Me.btnclear.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.Lime
        Me.btncari.Location = New System.Drawing.Point(356, 19)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(68, 34)
        Me.btncari.TabIndex = 32
        Me.btncari.Text = "&Tampilkan"
        Me.btncari.UseVisualStyleBackColor = False
        '
        'Form_customer
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(805, 606)
        Me.Controls.Add(Me.dgcustomer)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "Form_customer"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DATA CUSTOMER"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgcustomer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtnotes As System.Windows.Forms.TextBox
    Friend WithEvents txtcp As System.Windows.Forms.TextBox
    Friend WithEvents txtfax As System.Windows.Forms.TextBox
    Friend WithEvents lbl_notes As System.Windows.Forms.Label
    Friend WithEvents txttelepon As System.Windows.Forms.TextBox
    Friend WithEvents lbl_cp As System.Windows.Forms.Label
    Friend WithEvents txtkota As System.Windows.Forms.TextBox
    Friend WithEvents lbl_fax As System.Windows.Forms.Label
    Friend WithEvents txtnama As System.Windows.Forms.TextBox
    Friend WithEvents lbl_telepon As System.Windows.Forms.Label
    Friend WithEvents txtalamat As System.Windows.Forms.TextBox
    Friend WithEvents lbl_kota As System.Windows.Forms.Label
    Friend WithEvents l_alamat As System.Windows.Forms.Label
    Friend WithEvents l_nama As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents dgcustomer As System.Windows.Forms.DataGridView
    Friend WithEvents lblcari As System.Windows.Forms.Label
    Friend WithEvents txtnc As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents btnclear As System.Windows.Forms.Button
End Class
