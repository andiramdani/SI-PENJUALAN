﻿Public Class Form_menu

    Private Sub EXITToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EXITToolStripMenuItem.Click
        'KELUAR DARI PROGRAM UTAMA
        Dim x As String
        x = MsgBox("Apakah anda yakin ingin keluar dari Aplikasi", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "KONFIRMASI")
        If x = vbYes Then
            End
        End If
    End Sub

    Private Sub ABOUTToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ABOUTToolStripMenuItem.Click
        MsgBox("SISTEM INFORMASI PENJUALAN - CREATED BY WINDI - 2017", MsgBoxStyle.Information, "ABOUT")
    End Sub

    Private Sub MASTERUSERToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MASTERUSERToolStripMenuItem.Click
        'TAMPILKAN FORM MASTER USER 
        'Form_master_user.MdiParent = Me
        Form_master_user.Show()
        Form_master_user.Focus()
    End Sub
    Private Sub MASTERBARANGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MASTERBARANGToolStripMenuItem.Click
        'TAMPILKAN FORM MASTER BARANG 
        'Form_master_barang.MdiParent = Me
        Form_master_barang.Show()
        Form_master_barang.Focus()
    End Sub
    Private Sub Form_menu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'MsgBox("SELAMAT DATANG DI SISTEM INFORMASI PENJUALAN", MsgBoxStyle.Information, "WELCOME")
    End Sub

    Private Sub MASTERCUSTOMERToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MASTERCUSTOMERToolStripMenuItem.Click
        'TAMPILKAN FORM MASTER SUPPLIER 
        'Form_customer.MdiParent = Me
        Form_customer.Show()
        Form_customer.Focus()
    End Sub
    Private Sub PENGIRIMANBRGToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PENGIRIMANBRGToolStripMenuItem.Click

    End Sub

    Private Sub ORDERJUALToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ORDERJUALToolStripMenuItem.Click
        'TAMPILKAN FORM ORDER JUAL 
        'Form_order_jual.MdiParent = Me
        Form_order_jual.Show()
        Form_order_jual.Focus()
    End Sub

    Private Sub TAMBAHTRANSAKSIToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TAMBAHTRANSAKSIToolStripMenuItem.Click
        'TAMPILKAN FORM SALES INVOICE
        'Form_sales_invoice.MdiParent = Me
        Form_sales_invoice.Show()
        Form_sales_invoice.Focus()
    End Sub

    Private Sub LIHATDATAToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LIHATDATAToolStripMenuItem.Click
        'TAMPILKAN FORM DATA TRANSAKSI PENGIRIMAN BARANG 
        'Form_d_transaksi.MdiParent = Me
        Form_d_transaksi.Show()
        Form_d_transaksi.Focus()
    End Sub

    Private Sub CUSTOMERSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CUSTOMERSToolStripMenuItem.Click
        'TAMPILKAN FORM DATA CUSTOMER
        'Form_data_customer.MdiParent = Me
        Form_data_customer.Show()
        Form_data_customer.Focus()
    End Sub

    Private Sub FAKTURPAJAKToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles FAKTURPAJAKToolStripMenuItem.Click
        'TAMPILKAN FORM DELIVERY ORDER
        'Form_d_order.MdiParent = Me
        Form_d_order.Show()
        Form_d_order.Focus()
    End Sub

    Private Sub CASHToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CASHToolStripMenuItem.Click
        'TAMPILKAN FORM REPORT PENJUALAN TAHUNAN CASH
        'Form_report_sales_tahunan.MdiParent = Me
        Form_report_sales_tahunan.Show()
        Form_report_sales_tahunan.Focus()
    End Sub

    Private Sub CREDITToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CREDITToolStripMenuItem.Click
        'TAMPILKAN FORM REPORT PENJUALAN TAHUNAN CREDIT
        Form_report_sales_tahunan_credit.Show()
        Form_report_sales_tahunan_credit.Focus()
    End Sub

    Private Sub CREDITToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CREDITToolStripMenuItem1.Click
        'TAMPILKAN FORM REPORT PENJUALAN BULANAN CREDIT
        Form_report_sales_credit.Show()
        Form_report_sales_credit.Focus()
    End Sub

    Private Sub CASHToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CASHToolStripMenuItem1.Click
        'TAMPILKAN FORM REPORT PENJUALAN BULANAN CASH
        Form_report_sales.Show()
        Form_report_sales.Focus()
    End Sub
End Class