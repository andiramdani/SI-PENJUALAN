﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_order_jual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnnew = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dpdlv = New System.Windows.Forms.DateTimePicker()
        Me.dppo = New System.Windows.Forms.DateTimePicker()
        Me.cmbcustomer = New System.Windows.Forms.ComboBox()
        Me.txttharga = New System.Windows.Forms.TextBox()
        Me.lbl_tharga = New System.Windows.Forms.Label()
        Me.txtpay = New System.Windows.Forms.TextBox()
        Me.lbl_pay = New System.Windows.Forms.Label()
        Me.lbl_dpdlv = New System.Windows.Forms.Label()
        Me.lbl_dpo = New System.Windows.Forms.Label()
        Me.txtpo = New System.Windows.Forms.TextBox()
        Me.lbl_po = New System.Windows.Forms.Label()
        Me.lbl_customer = New System.Windows.Forms.Label()
        Me.dgorder_jual = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtnc = New System.Windows.Forms.TextBox()
        Me.btnclear = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.lblcari = New System.Windows.Forms.Label()
        Me.GroupBox10 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cmbstatus = New System.Windows.Forms.ComboBox()
        Me.lbl_status = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgorder_jual, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnupdate)
        Me.GroupBox2.Controls.Add(Me.btndelete)
        Me.GroupBox2.Controls.Add(Me.btnsave)
        Me.GroupBox2.Controls.Add(Me.btnnew)
        Me.GroupBox2.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(484, 80)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(136, 248)
        Me.GroupBox2.TabIndex = 26
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proses"
        '
        'btnupdate
        '
        Me.btnupdate.BackColor = System.Drawing.SystemColors.Control
        Me.btnupdate.Enabled = False
        Me.btnupdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Location = New System.Drawing.Point(26, 193)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(84, 36)
        Me.btnupdate.TabIndex = 22
        Me.btnupdate.Text = "&Update"
        Me.btnupdate.UseVisualStyleBackColor = False
        '
        'btndelete
        '
        Me.btndelete.BackColor = System.Drawing.SystemColors.Control
        Me.btndelete.Enabled = False
        Me.btndelete.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Location = New System.Drawing.Point(26, 142)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(84, 36)
        Me.btndelete.TabIndex = 21
        Me.btndelete.Text = "&Delete"
        Me.btndelete.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.SystemColors.Control
        Me.btnsave.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Location = New System.Drawing.Point(26, 89)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(84, 36)
        Me.btnsave.TabIndex = 20
        Me.btnsave.Text = "&Save"
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnnew
        '
        Me.btnnew.BackColor = System.Drawing.SystemColors.Control
        Me.btnnew.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnnew.Location = New System.Drawing.Point(26, 36)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(84, 36)
        Me.btnnew.TabIndex = 19
        Me.btnnew.Text = "&New"
        Me.btnnew.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dpdlv)
        Me.GroupBox1.Controls.Add(Me.dppo)
        Me.GroupBox1.Controls.Add(Me.cmbcustomer)
        Me.GroupBox1.Controls.Add(Me.txttharga)
        Me.GroupBox1.Controls.Add(Me.lbl_tharga)
        Me.GroupBox1.Controls.Add(Me.txtpay)
        Me.GroupBox1.Controls.Add(Me.lbl_pay)
        Me.GroupBox1.Controls.Add(Me.lbl_dpdlv)
        Me.GroupBox1.Controls.Add(Me.lbl_dpo)
        Me.GroupBox1.Controls.Add(Me.txtpo)
        Me.GroupBox1.Controls.Add(Me.lbl_po)
        Me.GroupBox1.Controls.Add(Me.lbl_customer)
        Me.GroupBox1.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(64, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(403, 316)
        Me.GroupBox1.TabIndex = 25
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Masukan - Data "
        '
        'dpdlv
        '
        Me.dpdlv.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dpdlv.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dpdlv.Location = New System.Drawing.Point(133, 153)
        Me.dpdlv.Name = "dpdlv"
        Me.dpdlv.Size = New System.Drawing.Size(144, 26)
        Me.dpdlv.TabIndex = 16
        '
        'dppo
        '
        Me.dppo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dppo.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dppo.Location = New System.Drawing.Point(133, 114)
        Me.dppo.Name = "dppo"
        Me.dppo.Size = New System.Drawing.Size(144, 26)
        Me.dppo.TabIndex = 15
        '
        'cmbcustomer
        '
        Me.cmbcustomer.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbcustomer.FormattingEnabled = True
        Me.cmbcustomer.Location = New System.Drawing.Point(133, 35)
        Me.cmbcustomer.Name = "cmbcustomer"
        Me.cmbcustomer.Size = New System.Drawing.Size(144, 27)
        Me.cmbcustomer.TabIndex = 13
        '
        'txttharga
        '
        Me.txttharga.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttharga.Location = New System.Drawing.Point(133, 231)
        Me.txttharga.Multiline = True
        Me.txttharga.Name = "txttharga"
        Me.txttharga.Size = New System.Drawing.Size(241, 28)
        Me.txttharga.TabIndex = 18
        '
        'lbl_tharga
        '
        Me.lbl_tharga.AutoSize = True
        Me.lbl_tharga.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_tharga.Location = New System.Drawing.Point(28, 234)
        Me.lbl_tharga.Name = "lbl_tharga"
        Me.lbl_tharga.Size = New System.Drawing.Size(80, 19)
        Me.lbl_tharga.TabIndex = 33
        Me.lbl_tharga.Text = "Total Harga"
        '
        'txtpay
        '
        Me.txtpay.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpay.Location = New System.Drawing.Point(133, 192)
        Me.txtpay.Multiline = True
        Me.txtpay.Name = "txtpay"
        Me.txtpay.Size = New System.Drawing.Size(241, 28)
        Me.txtpay.TabIndex = 17
        '
        'lbl_pay
        '
        Me.lbl_pay.AutoSize = True
        Me.lbl_pay.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_pay.Location = New System.Drawing.Point(28, 195)
        Me.lbl_pay.Name = "lbl_pay"
        Me.lbl_pay.Size = New System.Drawing.Size(61, 19)
        Me.lbl_pay.TabIndex = 33
        Me.lbl_pay.Text = "Payment"
        '
        'lbl_dpdlv
        '
        Me.lbl_dpdlv.AutoSize = True
        Me.lbl_dpdlv.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dpdlv.Location = New System.Drawing.Point(28, 156)
        Me.lbl_dpdlv.Name = "lbl_dpdlv"
        Me.lbl_dpdlv.Size = New System.Drawing.Size(59, 19)
        Me.lbl_dpdlv.TabIndex = 33
        Me.lbl_dpdlv.Text = "Delivery"
        '
        'lbl_dpo
        '
        Me.lbl_dpo.AutoSize = True
        Me.lbl_dpo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dpo.Location = New System.Drawing.Point(28, 117)
        Me.lbl_dpo.Name = "lbl_dpo"
        Me.lbl_dpo.Size = New System.Drawing.Size(38, 19)
        Me.lbl_dpo.TabIndex = 33
        Me.lbl_dpo.Text = "Date"
        '
        'txtpo
        '
        Me.txtpo.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpo.Location = New System.Drawing.Point(133, 75)
        Me.txtpo.Multiline = True
        Me.txtpo.Name = "txtpo"
        Me.txtpo.Size = New System.Drawing.Size(241, 28)
        Me.txtpo.TabIndex = 14
        '
        'lbl_po
        '
        Me.lbl_po.AutoSize = True
        Me.lbl_po.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_po.Location = New System.Drawing.Point(28, 78)
        Me.lbl_po.Name = "lbl_po"
        Me.lbl_po.Size = New System.Drawing.Size(54, 19)
        Me.lbl_po.TabIndex = 33
        Me.lbl_po.Text = "No PO"
        '
        'lbl_customer
        '
        Me.lbl_customer.AutoSize = True
        Me.lbl_customer.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_customer.Location = New System.Drawing.Point(28, 39)
        Me.lbl_customer.Name = "lbl_customer"
        Me.lbl_customer.Size = New System.Drawing.Size(68, 19)
        Me.lbl_customer.TabIndex = 33
        Me.lbl_customer.Text = "Customer"
        '
        'dgorder_jual
        '
        Me.dgorder_jual.AllowUserToAddRows = False
        Me.dgorder_jual.AllowUserToDeleteRows = False
        Me.dgorder_jual.AllowUserToOrderColumns = True
        Me.dgorder_jual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgorder_jual.Location = New System.Drawing.Point(23, 417)
        Me.dgorder_jual.Name = "dgorder_jual"
        Me.dgorder_jual.ReadOnly = True
        Me.dgorder_jual.Size = New System.Drawing.Size(792, 178)
        Me.dgorder_jual.TabIndex = 27
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtnc)
        Me.GroupBox3.Controls.Add(Me.btnclear)
        Me.GroupBox3.Controls.Add(Me.btncari)
        Me.GroupBox3.Controls.Add(Me.lblcari)
        Me.GroupBox3.Location = New System.Drawing.Point(289, 344)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(526, 62)
        Me.GroupBox3.TabIndex = 33
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Masukan Data Order Jual :"
        '
        'txtnc
        '
        Me.txtnc.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append
        Me.txtnc.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnc.Location = New System.Drawing.Point(148, 25)
        Me.txtnc.Name = "txtnc"
        Me.txtnc.Size = New System.Drawing.Size(201, 26)
        Me.txtnc.TabIndex = 34
        '
        'btnclear
        '
        Me.btnclear.BackColor = System.Drawing.Color.Red
        Me.btnclear.Location = New System.Drawing.Point(447, 19)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(68, 34)
        Me.btnclear.TabIndex = 33
        Me.btnclear.Text = "&Clear"
        Me.btnclear.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.LawnGreen
        Me.btncari.Location = New System.Drawing.Point(367, 19)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(68, 34)
        Me.btncari.TabIndex = 32
        Me.btncari.Text = "&Tampilkan"
        Me.btncari.UseVisualStyleBackColor = False
        '
        'lblcari
        '
        Me.lblcari.AutoSize = True
        Me.lblcari.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblcari.Location = New System.Drawing.Point(34, 26)
        Me.lblcari.Name = "lblcari"
        Me.lblcari.Size = New System.Drawing.Size(109, 19)
        Me.lblcari.TabIndex = 31
        Me.lblcari.Text = "Nama Customer"
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.Button1)
        Me.GroupBox10.Controls.Add(Me.cmbstatus)
        Me.GroupBox10.Controls.Add(Me.lbl_status)
        Me.GroupBox10.Location = New System.Drawing.Point(484, 20)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(293, 54)
        Me.GroupBox10.TabIndex = 46
        Me.GroupBox10.TabStop = False
        Me.GroupBox10.Text = "Status Transaksi :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Location = New System.Drawing.Point(202, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 24)
        Me.Button1.TabIndex = 36
        Me.Button1.Text = "&Save"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'cmbstatus
        '
        Me.cmbstatus.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbstatus.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbstatus.FormattingEnabled = True
        Me.cmbstatus.Items.AddRange(New Object() {"FINISHED", "UNFINISHED"})
        Me.cmbstatus.Location = New System.Drawing.Point(71, 18)
        Me.cmbstatus.MaxDropDownItems = 5
        Me.cmbstatus.Name = "cmbstatus"
        Me.cmbstatus.Size = New System.Drawing.Size(119, 25)
        Me.cmbstatus.Sorted = True
        Me.cmbstatus.TabIndex = 34
        '
        'lbl_status
        '
        Me.lbl_status.AutoSize = True
        Me.lbl_status.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_status.Location = New System.Drawing.Point(19, 21)
        Me.lbl_status.Name = "lbl_status"
        Me.lbl_status.Size = New System.Drawing.Size(46, 19)
        Me.lbl_status.TabIndex = 35
        Me.lbl_status.Text = "Status"
        '
        'Form_order_jual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 646)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.dgorder_jual)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form_order_jual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ORDER JUAL"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgorder_jual, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbcustomer As System.Windows.Forms.ComboBox
    Friend WithEvents txttharga As System.Windows.Forms.TextBox
    Friend WithEvents lbl_tharga As System.Windows.Forms.Label
    Friend WithEvents txtpay As System.Windows.Forms.TextBox
    Friend WithEvents lbl_pay As System.Windows.Forms.Label
    Friend WithEvents lbl_dpdlv As System.Windows.Forms.Label
    Friend WithEvents lbl_dpo As System.Windows.Forms.Label
    Friend WithEvents txtpo As System.Windows.Forms.TextBox
    Friend WithEvents lbl_po As System.Windows.Forms.Label
    Friend WithEvents lbl_customer As System.Windows.Forms.Label
    Friend WithEvents dpdlv As System.Windows.Forms.DateTimePicker
    Friend WithEvents dppo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dgorder_jual As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnclear As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents lblcari As System.Windows.Forms.Label
    Friend WithEvents txtnc As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents cmbstatus As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_status As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
