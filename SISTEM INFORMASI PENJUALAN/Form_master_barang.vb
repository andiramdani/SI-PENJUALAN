﻿Imports System.Data.OleDb
Public Class Form_master_barang
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Dim id As String
    Dim db As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub cari_db() 'FUNGSI CARI DATA DARI MASTER BARANG
        Try
            Dim dtpo As New AutoCompleteStringCollection()
            sql = "Select * From master_barang"
            Dim conn As New OleDbConnection(db)
            Dim cmd As New OleDbCommand(sql, conn)
            conn.Open()
            Dim dr As OleDbDataReader = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    dtpo.Add(dr("spek"))
                End While
            Else
                MessageBox.Show("MASTER BARANG BELUM ADA !!!!")
            End If
            dr.Close()
            With txtnb
                .AutoCompleteCustomSource = dtpo
                .AutoCompleteSource = AutoCompleteSource.CustomSource
                .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub tampilkan_dmb() 'FUNGSI TAMPILKAN PENCARIAN DATA MASTER BARANG

        sql = "SELECT * FROM master_barang where spek ='" & txtnb.Text & "'"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read() Then

            id = dtreader("id_barang")
            txtmerk.Text = dtreader("merk")
            txtjenis.Text = dtreader("jenis")
            txtspek.Text = dtreader("spek")
            txtdimensi.Text = dtreader("dimensi")
            cmbsat.Text = dtreader("satuan")
            txthj.Text = dtreader("harga_jual")
            txtnote.Text = dtreader("notes")

            MsgBox("DATA MASTER BARANG DITEMUKAN", MsgBoxStyle.Information, "SUCCESS")


            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save
            btnsave.Enabled = False

        Else
            MsgBox("DATA MASTER BARANG TIDAK DITEMUKAN !!!", MsgBoxStyle.Exclamation, "PERHATIAN")
            MsgBox("SILAHKAN ULANGI PENCARIAN !!!", MsgBoxStyle.Information, "PERHATIAN")

            bersih()
            txtnb.Focus()

        End If
        conn.Close()
    End Sub
    Sub daftar()

        conn.Open()
        sql = "select * from master_barang"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgbarang.DataSource = ttabel
        conn.Close()

        txtmerk.Focus()

    End Sub
    Sub id_barang() ' data otomatis untuk  id master barang
        sql = "select id_barang from master_barang order by id_barang desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_barang") + 1
        Else
            id = "1"
        End If
        conn.Close()
    End Sub
    Sub bersih() 'bersihkan isi form


        txtmerk.Text = "-"
        txtjenis.Text = "-"
        txtspek.Text = ""
        txtdimensi.Text = ""
        cmbsat.Text = ""
        txthj.Text = ""
        txtnote.Text = ""
        txtnb.Text = ""

        txtmerk.Focus()

    End Sub
    Sub simpan() ' simpan data barang
        'panggil koneksi ke database
        conn.Open()

        'variabel menampung data merk,jenis,spek,dimensi,satuan,notes
        Dim merk, jenis, spek, dimensi, satuan, nt As String

        Dim hj As Integer
        'variabel untuk harga jual

        id = id
        merk = txtmerk.Text
        jenis = txtjenis.Text
        spek = txtspek.Text
        dimensi = txtdimensi.Text
        satuan = cmbsat.Text
        hj = txthj.Text
        nt = txtnote.Text

        'PERINTAH SQL INPUT DATA KE DATABASE MASTER BARANG
        sql = "insert into master_barang values('" & id & "','" & merk & "','" & jenis & "','" & spek & "','" & dimensi & "','" & satuan & "','" & hj & "','" & nt & "')"
        'EKSEKUSI PERINTAH SQL
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Barang Berhasil Disimpan !!!", MsgBoxStyle.Information, "SUCCESS")

        id_barang()
    End Sub
    Sub hapus() ' hapus master barang
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data ini ? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then
            conn.Open()
            sql = "delete from master_barang where id_barang ='" & id & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Barang Berhasil Dihapus !!!", MsgBoxStyle.Information, "SUCCESS")

            bersih()

            cari_db()
        End If
    End Sub
    Sub ubah() 'ubah master barang
        conn.Open()
        sql = "update master_barang set merk='" & txtmerk.Text & "',jenis='" & txtjenis.Text & "',spek='" & txtspek.Text & "',dimensi='" & txtdimensi.Text & "',satuan='" & cmbsat.Text & "',harga_jual='" & txthj.Text & "',notes='" & txtnote.Text & "' where id_barang='" & id & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Barang Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")
        daftar()

        cari_db()
    End Sub
    Private Sub Form_master_barang_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        daftar()
        id_barang()
        cari_db()
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        bersih()
        daftar()
        id_barang()

        cari_db()

        'disable button delete dan update
        btndelete.Enabled = False
        btnupdate.Enabled = False

        'enable button save
        btnsave.Enabled = True
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If (txtspek.Text = "") Then
            MsgBox("SPEK BARANG MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtspek.Focus()
        ElseIf (txtdimensi.Text = "") Then
            MsgBox("DIMENSI BARANG MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtdimensi.Focus()
        ElseIf (cmbsat.Text = "") Then
            MsgBox("SATUAN BARANG MASIH KOSONG", MsgBoxStyle.Exclamation)
            cmbsat.Focus()
        ElseIf (txthj.Text = "") Then
            MsgBox("HARGA JUAL MASIH KOSONG", MsgBoxStyle.Exclamation)
            txthj.Focus()
        ElseIf (txtnote.Text = "") Then
            MsgBox("NOTES MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnote.Focus()
        Else
            simpan()
            daftar()
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        hapus()
        daftar()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        ubah()
        daftar()
    End Sub

    Private Sub dgbarang_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgbarang.CellContentClick
        Dim baris = dgbarang.CurrentRow.Index
        With dgbarang

            id = .Item(0, baris).Value
            txtmerk.Text = .Item(1, baris).Value
            txtjenis.Text = .Item(2, baris).Value
            txtspek.Text = .Item(3, baris).Value
            txtdimensi.Text = .Item(4, baris).Value
            cmbsat.Text = .Item(5, baris).Value
            txthj.Text = .Item(6, baris).Value
            txtnote.Text = .Item(7, baris).Value

            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save
            btnsave.Enabled = False
        End With
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        tampilkan_dmb()

    End Sub
End Class

