﻿Public Class Form_d_transaksi
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub daftar() ' data datagrid barang transaksi pengiriman barang
        conn.Open()

        'where id_p = '" & id & "'
        sql = "select * from pengiriman_brg"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgtpb.DataSource = ttabel

        conn.Close()
    End Sub
    Sub sortir_data() ' fungsi sortir data datagrid barang transaksi pengiriman barang - periode

        conn.Open()

        Dim d1 As String 'dari tanggal
        Dim d2 As String 'sampai tanggal

        d1 = Date.Parse(dp1.Text).ToString("yyyy'/'MM'/'dd")
        d2 = Date.Parse(dp2.Text).ToString("yyyy'/'MM'/'dd")
        sql = "select * from pengiriman_brg where delivery between #" & d1 & "# and #" & d2 & "# order by delivery asc"

        dtadapter = New OleDb.OleDbDataAdapter(Sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgtpb.DataSource = ttabel

        conn.Close()
    End Sub
    Private Sub Form_d_transaksi_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        daftar()
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        sortir_data()
    End Sub

    Private Sub btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        dp1.Text = ""
        dp2.Text = ""
        daftar()
    End Sub
End Class