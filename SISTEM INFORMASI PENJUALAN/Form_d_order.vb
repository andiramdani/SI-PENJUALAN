﻿Imports System.Data.OleDb
Public Class Form_d_order
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Public idb As Integer 'variabel public id data barang transaksi
    Public dn As String 'variabel public delivery no
    Dim db As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub dlv_no() 'FUNGSI TAMPILKAN DATA COMBO BOX DELIVERY NO

        sql = "select delivery_no from pengiriman_brg"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            cmbdn.Items.Add(dtreader("delivery_no"))
        End While
        conn.Close()
    End Sub
    Sub cari_dtpb() 'FUNGSI CARI DATA TRANSAKSI PENGIRIMAN BARANG
        Try
            Dim dtpo As New AutoCompleteStringCollection()
            sql = "Select * From pengiriman_brg"
            Dim conn As New OleDbConnection(db)
            Dim cmd As New OleDbCommand(sql, conn)
            conn.Open()
            Dim dr As OleDbDataReader = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    dtpo.Add(dr("delivery_no"))
                End While
            Else
                MessageBox.Show("DATA TRANSAKSI PENGIRIMAN BARANG MASIH KOSONG !!!!")
            End If
            dr.Close()
            With txtnd
                .AutoCompleteCustomSource = dtpo
                .AutoCompleteSource = AutoCompleteSource.CustomSource
                .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub dt_barang() 'FUNGSI TAMPILKAN DATA BARANG SUSUAI COMBOBOX NAMA BARANG YANG DIPILIH

        sql = "select * from master_barang where spek = '" & cmbnmbrg.Text & "' order by id_barang asc "
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            txtharga.Text = dtreader("harga_jual")
            txtsat.Text = dtreader("satuan")
        End While
        conn.Close()
    End Sub
    Sub nm_barang() 'FUNGSI TAMPILKAN DATA COMBO BOX NAMA BARANG - MASTER BARANG

        sql = "select distinct spek from master_barang"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            cmbnmbrg.Items.Add(dtreader("spek"))
        End While
        conn.Close()
    End Sub
    Sub tampilkan_db() 'FUNGSI TAMPILKAN DATA BARANG BERDASARKAN DELIVERY NO

        conn.Open()

        sql = "select * from db_transaksi where delivery_no = '" & txtnd.Text & "'"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgtpb.DataSource = ttabel

        conn.Close()


        'CARI DATA TOTAL PEMBAYARAN SEBELUMNYA

        sql = "select * from pengiriman_brg where delivery_no = '" & txtnd.Text & "' "
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then

            txtk.Text = dtreader("total_ps")
            txtspb.Text = dtreader("sisa_bayar")
            txtst.Text = dtreader("sub_total")
            txtppn.Text = dtreader("ppn")
            txttotal2.Text = dtreader("total")

            If (dtreader("jenis_bayar") = "CASH") Then
                checkboxcash.Checked = True
            ElseIf (dtreader("jenis_bayar") = "CREDIT") Then
                checkboxcredit.Checked = True
            Else
                checkboxcash.Checked = False
                checkboxcredit.Checked = False
            End If

        Else
            MsgBox("DATA TRANSAKSI TIDAK DITEMUKAN", MsgBoxStyle.Exclamation, "PERHATIAN")
            txtnd.Focus()
        End If
        conn.Close()

    End Sub
    Sub daftar() ' data datagrid barang transaksi pengiriman barang
        conn.Open()

        sql = "select * from db_transaksi"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgtpb.DataSource = ttabel

        conn.Close()
        cmbdn.Focus()
    End Sub
    Sub id_b() 'DATA ID DATA BARANG TRANSAKSI PENGIRIMAN BARANG
        sql = "select id_b from db_transaksi order by id_b desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            idb = dtreader("id_b") + 2
        Else
            idb = 110
        End If
        conn.Close()
    End Sub
    Sub clear()

        cmbdn.Text = ""
        cmbnmbrg.Text = ""
        txtsat.Text = ""
        txtjumlah.Text = ""
        txtharga.Text = ""
        txttotal.Text = ""

        txtk.Text = 0
        txtspb.Text = 0
        txtst.Text = 0
        txtppn.Text = 0
        txttotal2.Text = 0

        id_b()

    End Sub
    Sub simpan_lpb() ' simpan data list barang pengiriman barang
        'panggil koneksi database
        conn.Open()

        'variabel menampung data nama barang , satuan
        Dim nb, satuan As String

        'variabel menampung data jumlah,harga,total,stok akhir (data persediaan baru) 
        Dim jumlah, harga, total As Integer

        'variabel menampung data tanggal persediaan baru - hidden
        Dim tanggal As Date

        idb = idb
        dn = cmbdn.Text
        nb = cmbnmbrg.Text
        satuan = txtsat.Text

        jumlah = txtjumlah.Text
        harga = txtharga.Text

        total = txttotal.Text

        tanggal = dptransaki.Text

        'PERINTAH SQL INPUT DATA KE DATABASE DB_TRANSAKSI
        sql = "insert into db_transaksi values('" & idb & "','" & tanggal & "','" & nb & "','" & jumlah & "','" & satuan & "','" & harga & "','" & total & "','" & dn & "')"
        'EKSEKUSI PERINTAH SQL
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()

        conn.Close()
        id_b()

        MsgBox("Data Barang Transaksi Pengiriman Barang Berhasil Ditambahkan", MsgBoxStyle.Information, "SUCCESS")

        clear()
    End Sub
    Sub hapus_lpb() ' hapus salah satu data barang transaksi pengiriman
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data ini ? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then
            conn.Open()
            sql = "delete from db_transaksi where id_b ='" & idb & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()

            MsgBox("Data Barang Berhasil Dihapus !!!", MsgBoxStyle.Information, "SUCCESS")

            conn.Close()

            clear()
            daftar()

        End If
    End Sub
    Sub ubah_lpb() 'ubah data barang transaksi pengiriman
        conn.Open()

        Dim tanggal As Date
        tanggal = dptransaki.Text

        sql = "update db_transaksi set tanggal_t = '" & tanggal & "',spek ='" & cmbnmbrg.Text & "',qty ='" & txtjumlah.Text & "',sat='" & txtsat.Text & "',harga='" & txtharga.Text & "',jumlah='" & txttotal.Text & "' where id_b ='" & idb & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()

        conn.Close()
        MsgBox("Data Barang Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")

        daftar()
    End Sub
    Sub sub_total() 'FUNGSI TAMPILKAN DATA SUB TOTAL

        sql = "select sum(jumlah) as jumlah from db_transaksi where delivery_no = '" & dn & "' OR delivery_no = '" & txtnd.Text & "'  "
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            txtst.Text = dtreader("jumlah")
        Else
            txtst.Text = 0
        End If
        conn.Close()
    End Sub
    Sub ubah() 'ubah data transaksi pengiriman

        Dim jp As String

        If (checkboxcash.Checked = True) Then
            jp = checkboxcash.Text
        ElseIf (checkboxcredit.Checked = True) Then
            jp = checkboxcredit.Text
        Else
            jp = "-"
        End If

        conn.Open()

        sql = "update pengiriman_brg set jenis_bayar = '" & jp & "',total_ps ='" & txtk.Text & "',sisa_bayar ='" & txtspb.Text & "',sub_total ='" & txtst.Text & "',ppn ='" & txtppn.Text & "',total='" & txttotal2.Text & "' where delivery_no ='" & dn & "' OR delivery_no ='" & txtnd.Text & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()

        conn.Close()
        MsgBox("Data Transaksi Pengiriman Barang Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")

    End Sub
    Private Sub Form_d_order_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        id_b()
        dlv_no()
        cari_dtpb()
        nm_barang()
        daftar()
    End Sub
    Private Sub btnnew2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew2.Click
        clear()
        daftar()

        'disable button delete dan update
        btndelete.Enabled = False
        btnupdate.Enabled = False

        'enable button save, tanggal transaki
        btnsave2.Enabled = True
        dptransaki.Enabled = True

        checkboxcash.Checked = False
        checkboxcredit.Checked = False

        TabControl1.Show()
    End Sub
    Private Sub btnsave2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave2.Click
        If (cmbnmbrg.Text = "") Then
            MsgBox("NAMA BARANG BELUM ANDA PILIH", MsgBoxStyle.Exclamation)
            cmbnmbrg.Focus()
        ElseIf (txtjumlah.Text = "") Then
            MsgBox("JUMLAH BARANG MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtjumlah.Focus()
        Else
            simpan_lpb()
            daftar()
            'sub_total()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If (cmbnmbrg.Text = "") Then
            MsgBox("NAMA BARANG BELUM ANDA PILIH", MsgBoxStyle.Exclamation)
            cmbnmbrg.Focus()
        ElseIf (txtjumlah.Text = "") Then
            MsgBox("JUMLAH BARANG MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtjumlah.Focus()
        Else
            ubah_lpb()
            sub_total()
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        hapus_lpb()
        id_b()
    End Sub

    Private Sub cmbnmbrg_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbnmbrg.SelectedIndexChanged
        dt_barang()
    End Sub

    Private Sub txtjumlah_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtjumlah.TextChanged
        txttotal.Text = Val(txtharga.Text) * Val(txtjumlah.Text)
    End Sub

    Private Sub txtst_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtst.TextChanged
        txtppn.Text = Val(txtst.Text) * 0.1
    End Sub

    Private Sub txtppn_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtppn.TextChanged
        txttotal2.Text = Val(txtst.Text) + Val(txtppn.Text)
    End Sub

    Private Sub txtk_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtk.TextChanged
        txtspb.Text = Val(txttotal2.Text) - Val(txtk.Text)
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        tampilkan_db()
    End Sub

    Private Sub btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        txtnd.Text = ""
        clear()

        checkboxcash.Checked = False
        checkboxcredit.Checked = False

        TabControl1.Show()

        daftar()
    End Sub

    Private Sub dgtpb_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgtpb.CellContentClick
        Dim baris = dgtpb.CurrentRow.Index
        With dgtpb

            idb = .Item(0, baris).Value
            dptransaki.Text = .Item(1, baris).Value
            cmbnmbrg.Text = .Item(2, baris).Value
            txtjumlah.Text = .Item(3, baris).Value
            txtsat.Text = .Item(4, baris).Value
            txtharga.Text = .Item(5, baris).Value
            txttotal.Text = .Item(6, baris).Value
            cmbdn.Text = .Item(7, baris).Value

            dn = cmbdn.Text

            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save, tanggal transaki
            btnsave2.Enabled = False
            'dptransaki.Enabled = False

            sub_total()

            TabPage1.Show()
            TabPage1.Focus()

        End With
    End Sub
    Private Sub checkboxcash_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkboxcash.CheckedChanged
        If (checkboxcash.Checked = True) Then
            checkboxcredit.Enabled = False
            txtk.Enabled = False
            txtk.Text = 0
        Else
            checkboxcredit.Enabled = True
            txtk.Enabled = False
        End If
    End Sub

    Private Sub checkboxcredit_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkboxcredit.CheckedChanged
        If (checkboxcredit.Checked = True) Then
            checkboxcash.Enabled = False
            txtk.Enabled = True
        Else
            checkboxcash.Enabled = True
            txtk.Text = 0
            txtspb.Text = 0
            txtk.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ubah() ' update data pembayaran di table pengiriman barang
        daftar()
    End Sub

    Private Sub btnprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprint.Click
        'TAMPILKAN FORM SALELS INVOICE
        Form_s_invoice.CrystalReportViewer1.SelectionFormula = "{pengiriman_brg.delivery_no}='" & txtnd.Text & "' OR {pengiriman_brg.delivery_no}='" & cmbdn.Text & "'"
        Form_s_invoice.CrystalReportViewer1.Refresh()
        Form_s_invoice.Show()

        'TAMPILKAN FORM TANDA TERIMA
        Form_tanda_terima.CrystalReportViewer1.SelectionFormula = "{pengiriman_brg.delivery_no}='" & txtnd.Text & "' OR {pengiriman_brg.delivery_no}='" & cmbdn.Text & "'"
        Form_tanda_terima.CrystalReportViewer1.Refresh()
        Form_tanda_terima.Show()
    End Sub
End Class