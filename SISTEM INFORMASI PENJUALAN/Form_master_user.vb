﻿Public Class Form_master_user
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Dim id As String
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub daftar()
        conn.Open()
        sql = "select * from master_user"
        dtadapter = New oledb.oledbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dguser.DataSource = ttabel
        conn.Close()
        txtuser.Focus()
    End Sub
    Sub bersih()
        id = ""
        txtuser.Text = ""
        txtpass.Text = ""
        txtnama.Text = ""
        txtuser.Focus()
    End Sub
    Sub simpan()
        'panggil koneksi ke database
        conn.Open()

        'variabel menampung data username,password,nama lengkap
        Dim username, password, nama As String

        id = id
        username = txtuser.Text
        password = txtpass.Text
        nama = txtnama.Text

        'PERINTAH SQL INPUT DATA KE DATABASE MASTER USER
        sql = "insert into master_user values('" & id & "','" & username & "','" & password & "','" & nama & "')"
        'EKSEKUSI PERINTAH SQL
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data User Berhasil Disimpan", MsgBoxStyle.Information, "SUCCESS")
    End Sub
    Sub hapus()
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data ini ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then
            conn.Open()
            sql = "delete from master_user where id_user='" & id & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data User Berhasil Dihapus", MsgBoxStyle.Information, "SUCCESS")

            bersih()
        End If
    End Sub
    Sub ubah()
        conn.Open()
        sql = "update master_user set username='" & txtuser.Text & "',pass='" & txtpass.Text & "',nama='" & txtnama.Text & "' where id_user='" & id & "' "
        cmd = New oledb.oledbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data User Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")
    End Sub
    Sub cari()
        Dim x As String
        x = InputBox("Masukan Id User Yang Anda cari")
        sql = "select * from master_user where id_user = '" & x & "'"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_user")
            txtuser.Text = dtreader("username")
            txtpass.Text = dtreader("pass")
            txtnama.Text = dtreader("nama")
        Else
            MsgBox("Data User Tidak Ditemukan", MsgBoxStyle.RetryCancel)
        End If
        conn.Close()
    End Sub
    Sub id_user()
        sql = "select id_user from master_user order by id_user desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_user") + 1
        Else
            id = "10"
        End If
        conn.Close()
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        bersih()
        id_user()

        'disable button delete dan update
        btndelete.Enabled = False
        btnupdate.Enabled = False

        'enable button save
        btnsave.Enabled = True

        daftar()

    End Sub

    Private Sub Form_master_user_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        daftar()
        id_user()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If (txtuser.Text = "") Then
            MsgBox("USERNAME MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtuser.Focus()
        ElseIf (txtpass.Text = "") Then
            MsgBox("PASSWORD MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtpass.Focus()
        ElseIf (txtnama.Text = "") Then
            MsgBox("NAMA MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnama.Focus()
        Else
            simpan()
            bersih()
            daftar()
            id_user()
        End If
    End Sub

    Private Sub dguser_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dguser.CellContentClick
        Dim baris = dguser.CurrentRow.Index
        With dguser
            id = .Item(0, baris).Value
            txtuser.Text = .Item(1, baris).Value
            txtpass.Text = .Item(2, baris).Value
            txtnama.Text = .Item(3, baris).Value

            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save
            btnsave.Enabled = False
        End With
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        ubah()
        daftar()
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        hapus()
        daftar()
    End Sub
End Class
