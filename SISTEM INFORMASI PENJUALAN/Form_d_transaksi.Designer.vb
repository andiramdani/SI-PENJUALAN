﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_d_transaksi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lbldaritgl = New System.Windows.Forms.Label()
        Me.lblsampaitgl = New System.Windows.Forms.Label()
        Me.dp1 = New System.Windows.Forms.DateTimePicker()
        Me.dp2 = New System.Windows.Forms.DateTimePicker()
        Me.btnclear = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dgtpb = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgtpb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lbldaritgl)
        Me.GroupBox3.Controls.Add(Me.lblsampaitgl)
        Me.GroupBox3.Controls.Add(Me.dp1)
        Me.GroupBox3.Controls.Add(Me.dp2)
        Me.GroupBox3.Controls.Add(Me.btnclear)
        Me.GroupBox3.Controls.Add(Me.btncari)
        Me.GroupBox3.Location = New System.Drawing.Point(299, 79)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(547, 62)
        Me.GroupBox3.TabIndex = 34
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Sortir Data Transaksi  :"
        '
        'lbldaritgl
        '
        Me.lbldaritgl.AutoSize = True
        Me.lbldaritgl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbldaritgl.Location = New System.Drawing.Point(25, 29)
        Me.lbldaritgl.Name = "lbldaritgl"
        Me.lbldaritgl.Size = New System.Drawing.Size(33, 16)
        Me.lbldaritgl.TabIndex = 35
        Me.lbldaritgl.Text = "Dari"
        '
        'lblsampaitgl
        '
        Me.lblsampaitgl.AutoSize = True
        Me.lblsampaitgl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.lblsampaitgl.Location = New System.Drawing.Point(196, 29)
        Me.lblsampaitgl.Name = "lblsampaitgl"
        Me.lblsampaitgl.Size = New System.Drawing.Size(55, 16)
        Me.lblsampaitgl.TabIndex = 35
        Me.lblsampaitgl.Text = "Sampai"
        '
        'dp1
        '
        Me.dp1.CustomFormat = "YYYY/MM/DD"
        Me.dp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dp1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dp1.Location = New System.Drawing.Point(72, 26)
        Me.dp1.Name = "dp1"
        Me.dp1.Size = New System.Drawing.Size(110, 22)
        Me.dp1.TabIndex = 34
        '
        'dp2
        '
        Me.dp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dp2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dp2.Location = New System.Drawing.Point(265, 26)
        Me.dp2.Name = "dp2"
        Me.dp2.Size = New System.Drawing.Size(110, 22)
        Me.dp2.TabIndex = 34
        '
        'btnclear
        '
        Me.btnclear.BackColor = System.Drawing.Color.Red
        Me.btnclear.Location = New System.Drawing.Point(463, 24)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(68, 26)
        Me.btnclear.TabIndex = 33
        Me.btnclear.Text = "&Reset"
        Me.btnclear.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.Lime
        Me.btncari.Location = New System.Drawing.Point(389, 24)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(68, 26)
        Me.btncari.TabIndex = 32
        Me.btncari.Text = "&Tampilkan"
        Me.btncari.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgtpb)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 147)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(832, 387)
        Me.GroupBox1.TabIndex = 35
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Data Transaksi Pengiriman Barang :"
        '
        'dgtpb
        '
        Me.dgtpb.AllowUserToAddRows = False
        Me.dgtpb.AllowUserToDeleteRows = False
        Me.dgtpb.AllowUserToOrderColumns = True
        Me.dgtpb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgtpb.Location = New System.Drawing.Point(17, 36)
        Me.dgtpb.Name = "dgtpb"
        Me.dgtpb.ReadOnly = True
        Me.dgtpb.Size = New System.Drawing.Size(798, 331)
        Me.dgtpb.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.SystemColors.Menu
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(217, 21)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(427, 24)
        Me.Label3.TabIndex = 36
        Me.Label3.Text = "DATA TRANSAKSI PENGIRIMAN BARANG"
        '
        'Form_d_transaksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Menu
        Me.ClientSize = New System.Drawing.Size(860, 546)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "Form_d_transaksi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "DATA TRANSAKSI PENGIRIMAN BARANG"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgtpb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents btnclear As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgtpb As System.Windows.Forms.DataGridView
    Friend WithEvents lbldaritgl As System.Windows.Forms.Label
    Friend WithEvents lblsampaitgl As System.Windows.Forms.Label
    Friend WithEvents dp1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents dp2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
