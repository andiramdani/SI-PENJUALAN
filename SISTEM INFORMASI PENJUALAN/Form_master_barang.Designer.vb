﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_master_barang
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnnew = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmbsat = New System.Windows.Forms.ComboBox()
        Me.txtnote = New System.Windows.Forms.TextBox()
        Me.lblnote = New System.Windows.Forms.Label()
        Me.txthj = New System.Windows.Forms.TextBox()
        Me.lbl_hj = New System.Windows.Forms.Label()
        Me.lbl_sat = New System.Windows.Forms.Label()
        Me.txtdimensi = New System.Windows.Forms.TextBox()
        Me.lbl_dimensi = New System.Windows.Forms.Label()
        Me.txtspek = New System.Windows.Forms.TextBox()
        Me.l_spek = New System.Windows.Forms.Label()
        Me.txtmerk = New System.Windows.Forms.TextBox()
        Me.txtjenis = New System.Windows.Forms.TextBox()
        Me.l_jenis = New System.Windows.Forms.Label()
        Me.l_merk = New System.Windows.Forms.Label()
        Me.dgbarang = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtnb = New System.Windows.Forms.TextBox()
        Me.lbl_nb = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgbarang, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnupdate)
        Me.GroupBox2.Controls.Add(Me.btndelete)
        Me.GroupBox2.Controls.Add(Me.btnsave)
        Me.GroupBox2.Controls.Add(Me.btnnew)
        Me.GroupBox2.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(538, 92)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(153, 279)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Proses"
        '
        'btnupdate
        '
        Me.btnupdate.BackColor = System.Drawing.SystemColors.Control
        Me.btnupdate.Enabled = False
        Me.btnupdate.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Location = New System.Drawing.Point(27, 204)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(98, 36)
        Me.btnupdate.TabIndex = 16
        Me.btnupdate.Text = "&UPDATE"
        Me.btnupdate.UseVisualStyleBackColor = False
        '
        'btndelete
        '
        Me.btndelete.BackColor = System.Drawing.SystemColors.Control
        Me.btndelete.Enabled = False
        Me.btndelete.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btndelete.Location = New System.Drawing.Point(27, 153)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(98, 36)
        Me.btndelete.TabIndex = 114
        Me.btndelete.Text = "&DELETE"
        Me.btndelete.UseVisualStyleBackColor = False
        '
        'btnsave
        '
        Me.btnsave.BackColor = System.Drawing.SystemColors.Control
        Me.btnsave.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Location = New System.Drawing.Point(27, 100)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(98, 36)
        Me.btnsave.TabIndex = 12
        Me.btnsave.Text = "&SAVE"
        Me.btnsave.UseVisualStyleBackColor = False
        '
        'btnnew
        '
        Me.btnnew.BackColor = System.Drawing.SystemColors.Control
        Me.btnnew.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnnew.Location = New System.Drawing.Point(27, 47)
        Me.btnnew.Name = "btnnew"
        Me.btnnew.Size = New System.Drawing.Size(98, 36)
        Me.btnnew.TabIndex = 13
        Me.btnnew.Text = "&NEW"
        Me.btnnew.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmbsat)
        Me.GroupBox1.Controls.Add(Me.txtnote)
        Me.GroupBox1.Controls.Add(Me.lblnote)
        Me.GroupBox1.Controls.Add(Me.txthj)
        Me.GroupBox1.Controls.Add(Me.lbl_hj)
        Me.GroupBox1.Controls.Add(Me.lbl_sat)
        Me.GroupBox1.Controls.Add(Me.txtdimensi)
        Me.GroupBox1.Controls.Add(Me.lbl_dimensi)
        Me.GroupBox1.Controls.Add(Me.txtspek)
        Me.GroupBox1.Controls.Add(Me.l_spek)
        Me.GroupBox1.Controls.Add(Me.txtmerk)
        Me.GroupBox1.Controls.Add(Me.txtjenis)
        Me.GroupBox1.Controls.Add(Me.l_jenis)
        Me.GroupBox1.Controls.Add(Me.l_merk)
        Me.GroupBox1.Font = New System.Drawing.Font("Sitka Banner", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(18, 92)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(502, 321)
        Me.GroupBox1.TabIndex = 23
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Masukan - Data "
        '
        'cmbsat
        '
        Me.cmbsat.FormattingEnabled = True
        Me.cmbsat.Items.AddRange(New Object() {"pcs", "set"})
        Me.cmbsat.Location = New System.Drawing.Point(148, 178)
        Me.cmbsat.Name = "cmbsat"
        Me.cmbsat.Size = New System.Drawing.Size(241, 31)
        Me.cmbsat.TabIndex = 13
        '
        'txtnote
        '
        Me.txtnote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnote.Location = New System.Drawing.Point(148, 250)
        Me.txtnote.Multiline = True
        Me.txtnote.Name = "txtnote"
        Me.txtnote.Size = New System.Drawing.Size(334, 54)
        Me.txtnote.TabIndex = 15
        '
        'lblnote
        '
        Me.lblnote.AutoSize = True
        Me.lblnote.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnote.Location = New System.Drawing.Point(43, 250)
        Me.lblnote.Name = "lblnote"
        Me.lblnote.Size = New System.Drawing.Size(46, 19)
        Me.lblnote.TabIndex = 33
        Me.lblnote.Text = "Notes"
        '
        'txthj
        '
        Me.txthj.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txthj.Location = New System.Drawing.Point(148, 216)
        Me.txthj.Multiline = True
        Me.txthj.Name = "txthj"
        Me.txthj.Size = New System.Drawing.Size(241, 28)
        Me.txthj.TabIndex = 14
        '
        'lbl_hj
        '
        Me.lbl_hj.AutoSize = True
        Me.lbl_hj.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_hj.Location = New System.Drawing.Point(43, 221)
        Me.lbl_hj.Name = "lbl_hj"
        Me.lbl_hj.Size = New System.Drawing.Size(73, 19)
        Me.lbl_hj.TabIndex = 33
        Me.lbl_hj.Text = "Harga Jual"
        '
        'lbl_sat
        '
        Me.lbl_sat.AutoSize = True
        Me.lbl_sat.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sat.Location = New System.Drawing.Point(43, 184)
        Me.lbl_sat.Name = "lbl_sat"
        Me.lbl_sat.Size = New System.Drawing.Size(50, 19)
        Me.lbl_sat.TabIndex = 33
        Me.lbl_sat.Text = "Satuan"
        '
        'txtdimensi
        '
        Me.txtdimensi.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdimensi.Location = New System.Drawing.Point(148, 142)
        Me.txtdimensi.Multiline = True
        Me.txtdimensi.Name = "txtdimensi"
        Me.txtdimensi.Size = New System.Drawing.Size(241, 28)
        Me.txtdimensi.TabIndex = 12
        '
        'lbl_dimensi
        '
        Me.lbl_dimensi.AutoSize = True
        Me.lbl_dimensi.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_dimensi.Location = New System.Drawing.Point(43, 147)
        Me.lbl_dimensi.Name = "lbl_dimensi"
        Me.lbl_dimensi.Size = New System.Drawing.Size(57, 19)
        Me.lbl_dimensi.TabIndex = 33
        Me.lbl_dimensi.Text = "Dimensi"
        '
        'txtspek
        '
        Me.txtspek.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtspek.Location = New System.Drawing.Point(148, 109)
        Me.txtspek.Multiline = True
        Me.txtspek.Name = "txtspek"
        Me.txtspek.Size = New System.Drawing.Size(241, 28)
        Me.txtspek.TabIndex = 11
        '
        'l_spek
        '
        Me.l_spek.AutoSize = True
        Me.l_spek.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l_spek.Location = New System.Drawing.Point(43, 114)
        Me.l_spek.Name = "l_spek"
        Me.l_spek.Size = New System.Drawing.Size(41, 19)
        Me.l_spek.TabIndex = 33
        Me.l_spek.Text = "Spek"
        '
        'txtmerk
        '
        Me.txtmerk.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtmerk.Location = New System.Drawing.Point(148, 44)
        Me.txtmerk.Multiline = True
        Me.txtmerk.Name = "txtmerk"
        Me.txtmerk.Size = New System.Drawing.Size(241, 27)
        Me.txtmerk.TabIndex = 9
        Me.txtmerk.Text = "-"
        '
        'txtjenis
        '
        Me.txtjenis.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtjenis.Location = New System.Drawing.Point(148, 76)
        Me.txtjenis.Multiline = True
        Me.txtjenis.Name = "txtjenis"
        Me.txtjenis.Size = New System.Drawing.Size(241, 28)
        Me.txtjenis.TabIndex = 10
        Me.txtjenis.Text = "-"
        '
        'l_jenis
        '
        Me.l_jenis.AutoSize = True
        Me.l_jenis.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l_jenis.Location = New System.Drawing.Point(43, 81)
        Me.l_jenis.Name = "l_jenis"
        Me.l_jenis.Size = New System.Drawing.Size(38, 19)
        Me.l_jenis.TabIndex = 32
        Me.l_jenis.Text = "Jenis"
        '
        'l_merk
        '
        Me.l_merk.AutoSize = True
        Me.l_merk.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.l_merk.Location = New System.Drawing.Point(43, 48)
        Me.l_merk.Name = "l_merk"
        Me.l_merk.Size = New System.Drawing.Size(43, 19)
        Me.l_merk.TabIndex = 31
        Me.l_merk.Text = "Merk"
        '
        'dgbarang
        '
        Me.dgbarang.AllowUserToAddRows = False
        Me.dgbarang.AllowUserToDeleteRows = False
        Me.dgbarang.AllowUserToOrderColumns = True
        Me.dgbarang.Location = New System.Drawing.Point(18, 419)
        Me.dgbarang.Name = "dgbarang"
        Me.dgbarang.ReadOnly = True
        Me.dgbarang.Size = New System.Drawing.Size(850, 132)
        Me.dgbarang.TabIndex = 25
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.txtnb)
        Me.GroupBox3.Controls.Add(Me.lbl_nb)
        Me.GroupBox3.Location = New System.Drawing.Point(382, 26)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(493, 60)
        Me.GroupBox3.TabIndex = 26
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Cari Data Barang  :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Location = New System.Drawing.Point(366, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(111, 27)
        Me.Button1.TabIndex = 32
        Me.Button1.Text = "&Tampilkan"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtnb
        '
        Me.txtnb.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnb.Location = New System.Drawing.Point(197, 19)
        Me.txtnb.Name = "txtnb"
        Me.txtnb.Size = New System.Drawing.Size(150, 26)
        Me.txtnb.TabIndex = 9
        '
        'lbl_nb
        '
        Me.lbl_nb.AutoSize = True
        Me.lbl_nb.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nb.Location = New System.Drawing.Point(43, 22)
        Me.lbl_nb.Name = "lbl_nb"
        Me.lbl_nb.Size = New System.Drawing.Size(143, 19)
        Me.lbl_nb.TabIndex = 31
        Me.lbl_nb.Text = "Masukan spek barang"
        '
        'Form_master_barang
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(887, 565)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.dgbarang)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form_master_barang"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MASTER BARANG"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgbarang, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnnew As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txthj As System.Windows.Forms.TextBox
    Friend WithEvents lbl_hj As System.Windows.Forms.Label
    Friend WithEvents lbl_sat As System.Windows.Forms.Label
    Friend WithEvents txtdimensi As System.Windows.Forms.TextBox
    Friend WithEvents lbl_dimensi As System.Windows.Forms.Label
    Friend WithEvents txtspek As System.Windows.Forms.TextBox
    Friend WithEvents l_spek As System.Windows.Forms.Label
    Friend WithEvents txtmerk As System.Windows.Forms.TextBox
    Friend WithEvents txtjenis As System.Windows.Forms.TextBox
    Friend WithEvents l_jenis As System.Windows.Forms.Label
    Friend WithEvents l_merk As System.Windows.Forms.Label
    Friend WithEvents dgbarang As System.Windows.Forms.DataGridView
    Friend WithEvents cmbsat As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtnb As System.Windows.Forms.TextBox
    Friend WithEvents lbl_nb As System.Windows.Forms.Label
    Friend WithEvents txtnote As System.Windows.Forms.TextBox
    Friend WithEvents lblnote As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
