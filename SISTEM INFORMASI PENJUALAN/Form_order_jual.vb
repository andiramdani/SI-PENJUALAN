﻿Imports System.Data.OleDb
Public Class Form_order_jual
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Dim id As String
    Dim db As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub cari_doj() 'FUNGSI CARI DATA ORDER JUAL
        Try
            Dim dtpo As New AutoCompleteStringCollection()
            sql = "Select * From order_jual"
            Dim conn As New OleDbConnection(db)
            Dim cmd As New OleDbCommand(sql, conn)
            conn.Open()
            Dim dr As OleDbDataReader = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    dtpo.Add(dr("customer"))
                End While
            Else
                MessageBox.Show("DATA ORDER JUAL MASIH KOSONG !!!!")
            End If
            dr.Close()
            With txtnc
                .AutoCompleteCustomSource = dtpo
                .AutoCompleteSource = AutoCompleteSource.CustomSource
                .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub tampilkan_doj() 'FUNGSI TAMPILKAN PENCARIAN DATA ORDER JUAL

        conn.Open()

        sql = "select * from order_jual where customer LIKE '" + txtnc.Text + "%'"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgorder_jual.DataSource = ttabel
        conn.Close()
       
    End Sub
    Sub nama_customer() 'FUNGSI TAMPILKAN DATA COMBOBOX CUSTOMER

        sql = "select customer from customer"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            cmbcustomer.Items.Add(dtreader("customer"))
        End While
        conn.Close()
    End Sub
    Sub daftar()

        conn.Open()
        sql = "select * from order_jual"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgorder_jual.DataSource = ttabel
        conn.Close()

    End Sub
    Sub id_oj() ' data otomatis untuk  id ORDER JUAL
        sql = "select id_order from order_jual order by id_order desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_order") + 1
        Else
            id = "111"
        End If
        conn.Close()
    End Sub
    Sub bersih() 'bersihkan isi form

        cmbcustomer.Text = ""
        cmbcustomer.Items.Clear()

        txtpo.Text = ""
        dppo.Text = ""
        dpdlv.Text = ""
        txtpay.Text = ""
        txttharga.Text = ""
        cmbstatus.Text = ""

        txtnc.Text = ""

        cmbcustomer.Focus()

    End Sub
    Sub simpan() ' simpan data order jual
        'panggil koneksi ke database
        conn.Open()

        'variabel menampung data customer,no po
        Dim c, po As String

        'variabel menampung data tanggal po dan kirim
        Dim dpo, ddlv As Date

        Dim pay, tharga As Integer
        'variabel untuk payment,total harga

        id = id
        c = cmbcustomer.Text
        po = txtpo.Text
        dpo = dppo.Text
        ddlv = dpdlv.Text
        pay = txtpay.Text
        tharga = txttharga.Text

        'PERINTAH SQL INPUT DATA KE DATABASE ORDER JUAL
        sql = "insert into order_jual values('" & id & "','" & c & "','" & po & "','" & dpo & "','" & ddlv & "','" & pay & "','" & tharga & "','-')"
        'EKSEKUSI PERINTAH SQL
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Order Jual Berhasil Disimpan !!!", MsgBoxStyle.Information, "SUCCESS")

        id_oj()
    End Sub
    Sub hapus() ' hapus order jual
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data ini ? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then
            conn.Open()
            sql = "delete from order_jual where id_order ='" & id & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()
            MsgBox("Data Order Jual Berhasil Dihapus !!!", MsgBoxStyle.Information, "SUCCESS")

            bersih()

            cari_doj()
        End If
    End Sub
    Sub ubah_status() 'ubah STATUS ORDER JUAL
        conn.Open()

        sql = "update order_jual set status = '" & cmbstatus.Text & "' where id_order='" & id & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Status Order Jual Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")
        daftar()

    End Sub
    Sub ubah() 'ubah ORDER JUAL
        conn.Open()
        sql = "update order_jual set customer ='" & cmbcustomer.Text & "',no_po='" & txtpo.Text & "',tgl_po='" & dppo.Text & "',tgl_kirim='" & dpdlv.Text & "',payment='" & txtpay.Text & "',total_harga='" & txttharga.Text & "' where id_order='" & id & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Order Jual Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")
        daftar()

        cari_doj()
    End Sub
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        bersih()
        id_oj()

        'disable button delete dan update
        btndelete.Enabled = False
        btnupdate.Enabled = False

        'enable button save
        btnsave.Enabled = True

        daftar()
        nama_customer()

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If (cmbcustomer.Text = "") Then
            MsgBox("NAMA CUSTOMER BELUM ANDA ISI", MsgBoxStyle.Exclamation)
            cmbcustomer.Focus()
        ElseIf (txtpo.Text = "") Then
            MsgBox("NO PO MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtpo.Focus()
        ElseIf (dppo.Text = "") Then
            MsgBox("TANGGAL PO BELUM ANDA PILIH", MsgBoxStyle.Exclamation)
            dppo.Focus()
        ElseIf (dpdlv.Text = "") Then
            MsgBox("TANGGAL KIRIM BELUM ANDA PILIH", MsgBoxStyle.Exclamation)
            dpdlv.Focus()
        ElseIf (txtpay.Text = "") Then
            MsgBox("PAYMENT BELUM ANDA ISI", MsgBoxStyle.Exclamation)
            txtpay.Focus()
        ElseIf (txttharga.Text = "") Then
            MsgBox("TOTAL HARGA BELUM ANDA ISI", MsgBoxStyle.Exclamation)
            txttharga.Focus()
        Else
            simpan()
            bersih()
            daftar()
            id_oj()
            cari_doj()
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        hapus()
        daftar()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        ubah()
        daftar()
    End Sub

    Private Sub Form_order_jual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        id_oj()
        nama_customer()
        cari_doj()
        daftar()
    End Sub

    Private Sub dgorder_jual_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgorder_jual.CellContentClick
        Dim baris = dgorder_jual.CurrentRow.Index
        With dgorder_jual

            id = .Item(0, baris).Value
            cmbcustomer.Text = .Item(1, baris).Value
            txtpo.Text = .Item(2, baris).Value
            dppo.Text = .Item(3, baris).Value
            dpdlv.Text = .Item(4, baris).Value
            txtpay.Text = .Item(5, baris).Value
            txttharga.Text = .Item(6, baris).Value

            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save
            btnsave.Enabled = False
        End With
    End Sub

    Private Sub btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        bersih()
        cari_doj()
        daftar()
        nama_customer()
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        tampilkan_doj()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ubah_status()
    End Sub
End Class