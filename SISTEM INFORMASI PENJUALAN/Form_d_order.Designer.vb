﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_d_order
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnnew2 = New System.Windows.Forms.Button()
        Me.btnsave2 = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.lbl_delvno = New System.Windows.Forms.Label()
        Me.cmbdn = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txttotal = New System.Windows.Forms.TextBox()
        Me.lbl_totalh = New System.Windows.Forms.Label()
        Me.dptransaki = New System.Windows.Forms.DateTimePicker()
        Me.txtharga = New System.Windows.Forms.TextBox()
        Me.lbl_harga = New System.Windows.Forms.Label()
        Me.txtsat = New System.Windows.Forms.TextBox()
        Me.lbl_sat = New System.Windows.Forms.Label()
        Me.txtjumlah = New System.Windows.Forms.TextBox()
        Me.lbl_jumlah = New System.Windows.Forms.Label()
        Me.cmbnmbrg = New System.Windows.Forms.ComboBox()
        Me.lbl_nmbrg = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.dgtpb = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.txttotal2 = New System.Windows.Forms.TextBox()
        Me.lbltotal2 = New System.Windows.Forms.Label()
        Me.txtppn = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtst = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox8 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.checkboxcredit = New System.Windows.Forms.CheckBox()
        Me.txtspb = New System.Windows.Forms.TextBox()
        Me.lblsp = New System.Windows.Forms.Label()
        Me.checkboxcash = New System.Windows.Forms.CheckBox()
        Me.txtk = New System.Windows.Forms.TextBox()
        Me.lblkredit = New System.Windows.Forms.Label()
        Me.GroupBox9 = New System.Windows.Forms.GroupBox()
        Me.btnprint = New System.Windows.Forms.Button()
        Me.btnclear = New System.Windows.Forms.Button()
        Me.btncari = New System.Windows.Forms.Button()
        Me.txtnd = New System.Windows.Forms.TextBox()
        Me.lblnd = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgtpb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(23, 100)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(766, 342)
        Me.TabControl1.TabIndex = 41
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.GroupBox6)
        Me.TabPage1.Controls.Add(Me.GroupBox5)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(758, 316)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Tambah Data Barang "
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.btndelete)
        Me.GroupBox6.Controls.Add(Me.btnupdate)
        Me.GroupBox6.Controls.Add(Me.btnnew2)
        Me.GroupBox6.Controls.Add(Me.btnsave2)
        Me.GroupBox6.Location = New System.Drawing.Point(357, 252)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(395, 58)
        Me.GroupBox6.TabIndex = 43
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Aksi Data Barang"
        '
        'btndelete
        '
        Me.btndelete.Enabled = False
        Me.btndelete.Location = New System.Drawing.Point(306, 19)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(72, 28)
        Me.btndelete.TabIndex = 0
        Me.btndelete.Text = "&Delete"
        Me.btndelete.UseVisualStyleBackColor = True
        '
        'btnupdate
        '
        Me.btnupdate.Enabled = False
        Me.btnupdate.Location = New System.Drawing.Point(223, 19)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(72, 28)
        Me.btnupdate.TabIndex = 0
        Me.btnupdate.Text = "&Update"
        Me.btnupdate.UseVisualStyleBackColor = True
        '
        'btnnew2
        '
        Me.btnnew2.Location = New System.Drawing.Point(57, 19)
        Me.btnnew2.Name = "btnnew2"
        Me.btnnew2.Size = New System.Drawing.Size(72, 28)
        Me.btnnew2.TabIndex = 0
        Me.btnnew2.Text = "&New"
        Me.btnnew2.UseVisualStyleBackColor = True
        '
        'btnsave2
        '
        Me.btnsave2.Location = New System.Drawing.Point(140, 19)
        Me.btnsave2.Name = "btnsave2"
        Me.btnsave2.Size = New System.Drawing.Size(72, 28)
        Me.btnsave2.TabIndex = 0
        Me.btnsave2.Text = "&Save"
        Me.btnsave2.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.lbl_delvno)
        Me.GroupBox5.Controls.Add(Me.cmbdn)
        Me.GroupBox5.Controls.Add(Me.Label3)
        Me.GroupBox5.Controls.Add(Me.txttotal)
        Me.GroupBox5.Controls.Add(Me.lbl_totalh)
        Me.GroupBox5.Controls.Add(Me.dptransaki)
        Me.GroupBox5.Controls.Add(Me.txtharga)
        Me.GroupBox5.Controls.Add(Me.lbl_harga)
        Me.GroupBox5.Controls.Add(Me.txtsat)
        Me.GroupBox5.Controls.Add(Me.lbl_sat)
        Me.GroupBox5.Controls.Add(Me.txtjumlah)
        Me.GroupBox5.Controls.Add(Me.lbl_jumlah)
        Me.GroupBox5.Controls.Add(Me.cmbnmbrg)
        Me.GroupBox5.Controls.Add(Me.lbl_nmbrg)
        Me.GroupBox5.Location = New System.Drawing.Point(10, 7)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(739, 239)
        Me.GroupBox5.TabIndex = 42
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Data Barang :"
        '
        'lbl_delvno
        '
        Me.lbl_delvno.AutoSize = True
        Me.lbl_delvno.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_delvno.Location = New System.Drawing.Point(36, 40)
        Me.lbl_delvno.Name = "lbl_delvno"
        Me.lbl_delvno.Size = New System.Drawing.Size(87, 19)
        Me.lbl_delvno.TabIndex = 50
        Me.lbl_delvno.Text = "Delivery No."
        '
        'cmbdn
        '
        Me.cmbdn.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cmbdn.Font = New System.Drawing.Font("Times New Roman", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbdn.FormattingEnabled = True
        Me.cmbdn.Location = New System.Drawing.Point(163, 40)
        Me.cmbdn.MaxDropDownItems = 5
        Me.cmbdn.Name = "cmbdn"
        Me.cmbdn.Size = New System.Drawing.Size(194, 25)
        Me.cmbdn.Sorted = True
        Me.cmbdn.TabIndex = 49
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(36, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(116, 19)
        Me.Label3.TabIndex = 48
        Me.Label3.Text = "Tanggal Transaksi"
        '
        'txttotal
        '
        Me.txttotal.Enabled = False
        Me.txttotal.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttotal.Location = New System.Drawing.Point(446, 181)
        Me.txttotal.Multiline = True
        Me.txttotal.Name = "txttotal"
        Me.txttotal.Size = New System.Drawing.Size(143, 27)
        Me.txttotal.TabIndex = 39
        Me.txttotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_totalh
        '
        Me.lbl_totalh.AutoSize = True
        Me.lbl_totalh.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_totalh.Location = New System.Drawing.Point(353, 184)
        Me.lbl_totalh.Name = "lbl_totalh"
        Me.lbl_totalh.Size = New System.Drawing.Size(80, 19)
        Me.lbl_totalh.TabIndex = 35
        Me.lbl_totalh.Text = "Total Harga"
        '
        'dptransaki
        '
        Me.dptransaki.Font = New System.Drawing.Font("Times New Roman", 12.0!)
        Me.dptransaki.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dptransaki.Location = New System.Drawing.Point(163, 71)
        Me.dptransaki.Name = "dptransaki"
        Me.dptransaki.Size = New System.Drawing.Size(104, 26)
        Me.dptransaki.TabIndex = 47
        '
        'txtharga
        '
        Me.txtharga.Enabled = False
        Me.txtharga.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtharga.Location = New System.Drawing.Point(160, 136)
        Me.txtharga.Multiline = True
        Me.txtharga.Name = "txtharga"
        Me.txtharga.Size = New System.Drawing.Size(143, 27)
        Me.txtharga.TabIndex = 39
        Me.txtharga.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbl_harga
        '
        Me.lbl_harga.AutoSize = True
        Me.lbl_harga.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_harga.Location = New System.Drawing.Point(36, 139)
        Me.lbl_harga.Name = "lbl_harga"
        Me.lbl_harga.Size = New System.Drawing.Size(46, 19)
        Me.lbl_harga.TabIndex = 35
        Me.lbl_harga.Text = "Harga"
        '
        'txtsat
        '
        Me.txtsat.Enabled = False
        Me.txtsat.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsat.Location = New System.Drawing.Point(446, 139)
        Me.txtsat.Multiline = True
        Me.txtsat.Name = "txtsat"
        Me.txtsat.Size = New System.Drawing.Size(143, 27)
        Me.txtsat.TabIndex = 39
        '
        'lbl_sat
        '
        Me.lbl_sat.AutoSize = True
        Me.lbl_sat.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_sat.Location = New System.Drawing.Point(353, 139)
        Me.lbl_sat.Name = "lbl_sat"
        Me.lbl_sat.Size = New System.Drawing.Size(50, 19)
        Me.lbl_sat.TabIndex = 35
        Me.lbl_sat.Text = "Satuan"
        '
        'txtjumlah
        '
        Me.txtjumlah.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtjumlah.Location = New System.Drawing.Point(160, 181)
        Me.txtjumlah.Multiline = True
        Me.txtjumlah.Name = "txtjumlah"
        Me.txtjumlah.Size = New System.Drawing.Size(143, 27)
        Me.txtjumlah.TabIndex = 39
        '
        'lbl_jumlah
        '
        Me.lbl_jumlah.AutoSize = True
        Me.lbl_jumlah.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_jumlah.Location = New System.Drawing.Point(33, 181)
        Me.lbl_jumlah.Name = "lbl_jumlah"
        Me.lbl_jumlah.Size = New System.Drawing.Size(50, 19)
        Me.lbl_jumlah.TabIndex = 35
        Me.lbl_jumlah.Text = "Jumlah"
        '
        'cmbnmbrg
        '
        Me.cmbnmbrg.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmbnmbrg.FormattingEnabled = True
        Me.cmbnmbrg.Location = New System.Drawing.Point(163, 103)
        Me.cmbnmbrg.Name = "cmbnmbrg"
        Me.cmbnmbrg.Size = New System.Drawing.Size(426, 23)
        Me.cmbnmbrg.TabIndex = 34
        '
        'lbl_nmbrg
        '
        Me.lbl_nmbrg.AutoSize = True
        Me.lbl_nmbrg.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nmbrg.Location = New System.Drawing.Point(36, 109)
        Me.lbl_nmbrg.Name = "lbl_nmbrg"
        Me.lbl_nmbrg.Size = New System.Drawing.Size(93, 19)
        Me.lbl_nmbrg.TabIndex = 35
        Me.lbl_nmbrg.Text = "Nama Barang"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox3)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(758, 316)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Data Barang "
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgtpb)
        Me.GroupBox3.Location = New System.Drawing.Point(27, 17)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(705, 218)
        Me.GroupBox3.TabIndex = 39
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Details :"
        '
        'dgtpb
        '
        Me.dgtpb.AllowUserToAddRows = False
        Me.dgtpb.AllowUserToDeleteRows = False
        Me.dgtpb.AllowUserToOrderColumns = True
        Me.dgtpb.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgtpb.Location = New System.Drawing.Point(30, 26)
        Me.dgtpb.Name = "dgtpb"
        Me.dgtpb.ReadOnly = True
        Me.dgtpb.Size = New System.Drawing.Size(645, 176)
        Me.dgtpb.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.GroupBox7)
        Me.TabPage3.Controls.Add(Me.GroupBox8)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(758, 316)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Data Pembayaran"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.txttotal2)
        Me.GroupBox7.Controls.Add(Me.lbltotal2)
        Me.GroupBox7.Controls.Add(Me.txtppn)
        Me.GroupBox7.Controls.Add(Me.Label2)
        Me.GroupBox7.Controls.Add(Me.txtst)
        Me.GroupBox7.Controls.Add(Me.Label1)
        Me.GroupBox7.Location = New System.Drawing.Point(440, 165)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(293, 138)
        Me.GroupBox7.TabIndex = 45
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Details"
        '
        'txttotal2
        '
        Me.txttotal2.Enabled = False
        Me.txttotal2.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txttotal2.Location = New System.Drawing.Point(132, 94)
        Me.txttotal2.Multiline = True
        Me.txttotal2.Name = "txttotal2"
        Me.txttotal2.Size = New System.Drawing.Size(143, 27)
        Me.txttotal2.TabIndex = 39
        Me.txttotal2.Text = "0"
        Me.txttotal2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lbltotal2
        '
        Me.lbltotal2.AutoSize = True
        Me.lbltotal2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal2.Location = New System.Drawing.Point(80, 97)
        Me.lbltotal2.Name = "lbltotal2"
        Me.lbltotal2.Size = New System.Drawing.Size(37, 20)
        Me.lbltotal2.TabIndex = 35
        Me.lbltotal2.Text = "Total"
        '
        'txtppn
        '
        Me.txtppn.Enabled = False
        Me.txtppn.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtppn.Location = New System.Drawing.Point(132, 61)
        Me.txtppn.Multiline = True
        Me.txtppn.Name = "txtppn"
        Me.txtppn.Size = New System.Drawing.Size(143, 27)
        Me.txtppn.TabIndex = 39
        Me.txtppn.Text = "0"
        Me.txtppn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(54, 64)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 20)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "PPN 10%"
        '
        'txtst
        '
        Me.txtst.Enabled = False
        Me.txtst.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtst.Location = New System.Drawing.Point(132, 28)
        Me.txtst.Multiline = True
        Me.txtst.Name = "txtst"
        Me.txtst.Size = New System.Drawing.Size(143, 27)
        Me.txtst.TabIndex = 39
        Me.txtst.Text = "0"
        Me.txtst.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(59, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 20)
        Me.Label1.TabIndex = 35
        Me.Label1.Text = "Sub Total"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button1)
        Me.GroupBox8.Controls.Add(Me.checkboxcredit)
        Me.GroupBox8.Controls.Add(Me.txtspb)
        Me.GroupBox8.Controls.Add(Me.lblsp)
        Me.GroupBox8.Controls.Add(Me.checkboxcash)
        Me.GroupBox8.Controls.Add(Me.txtk)
        Me.GroupBox8.Controls.Add(Me.lblkredit)
        Me.GroupBox8.Location = New System.Drawing.Point(18, 10)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(715, 149)
        Me.GroupBox8.TabIndex = 44
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Status Pembayaran :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Location = New System.Drawing.Point(594, 19)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(103, 38)
        Me.Button1.TabIndex = 46
        Me.Button1.Text = "Update"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'checkboxcredit
        '
        Me.checkboxcredit.AutoSize = True
        Me.checkboxcredit.Location = New System.Drawing.Point(631, 73)
        Me.checkboxcredit.Name = "checkboxcredit"
        Me.checkboxcredit.Size = New System.Drawing.Size(66, 17)
        Me.checkboxcredit.TabIndex = 41
        Me.checkboxcredit.Text = "CREDIT"
        Me.checkboxcredit.UseVisualStyleBackColor = True
        '
        'txtspb
        '
        Me.txtspb.Enabled = False
        Me.txtspb.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtspb.Location = New System.Drawing.Point(554, 105)
        Me.txtspb.Multiline = True
        Me.txtspb.Name = "txtspb"
        Me.txtspb.Size = New System.Drawing.Size(143, 27)
        Me.txtspb.TabIndex = 42
        Me.txtspb.Text = "0"
        Me.txtspb.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblsp
        '
        Me.lblsp.AutoSize = True
        Me.lblsp.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsp.Location = New System.Drawing.Point(428, 107)
        Me.lblsp.Name = "lblsp"
        Me.lblsp.Size = New System.Drawing.Size(114, 20)
        Me.lblsp.TabIndex = 41
        Me.lblsp.Text = "Sisa Pembayaran"
        '
        'checkboxcash
        '
        Me.checkboxcash.AutoSize = True
        Me.checkboxcash.Location = New System.Drawing.Point(570, 73)
        Me.checkboxcash.Name = "checkboxcash"
        Me.checkboxcash.Size = New System.Drawing.Size(55, 17)
        Me.checkboxcash.TabIndex = 41
        Me.checkboxcash.Text = "CASH"
        Me.checkboxcash.UseVisualStyleBackColor = True
        '
        'txtk
        '
        Me.txtk.Enabled = False
        Me.txtk.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtk.Location = New System.Drawing.Point(269, 104)
        Me.txtk.Multiline = True
        Me.txtk.Name = "txtk"
        Me.txtk.Size = New System.Drawing.Size(143, 27)
        Me.txtk.TabIndex = 43
        Me.txtk.Text = "0"
        Me.txtk.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'lblkredit
        '
        Me.lblkredit.AutoSize = True
        Me.lblkredit.Font = New System.Drawing.Font("Arial Narrow", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblkredit.Location = New System.Drawing.Point(133, 106)
        Me.lblkredit.Name = "lblkredit"
        Me.lblkredit.Size = New System.Drawing.Size(130, 20)
        Me.lblkredit.TabIndex = 40
        Me.lblkredit.Text = "Jumlah Pembayaran"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.btnprint)
        Me.GroupBox9.Controls.Add(Me.btnclear)
        Me.GroupBox9.Controls.Add(Me.btncari)
        Me.GroupBox9.Controls.Add(Me.txtnd)
        Me.GroupBox9.Controls.Add(Me.lblnd)
        Me.GroupBox9.Location = New System.Drawing.Point(182, 12)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(603, 62)
        Me.GroupBox9.TabIndex = 43
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Cari Data Transaksi :"
        '
        'btnprint
        '
        Me.btnprint.Location = New System.Drawing.Point(510, 19)
        Me.btnprint.Name = "btnprint"
        Me.btnprint.Size = New System.Drawing.Size(68, 34)
        Me.btnprint.TabIndex = 44
        Me.btnprint.Text = "&Print"
        Me.btnprint.UseVisualStyleBackColor = True
        '
        'btnclear
        '
        Me.btnclear.BackColor = System.Drawing.Color.Red
        Me.btnclear.Location = New System.Drawing.Point(436, 19)
        Me.btnclear.Name = "btnclear"
        Me.btnclear.Size = New System.Drawing.Size(68, 34)
        Me.btnclear.TabIndex = 33
        Me.btnclear.Text = "&Clear"
        Me.btnclear.UseVisualStyleBackColor = False
        '
        'btncari
        '
        Me.btncari.BackColor = System.Drawing.Color.Lime
        Me.btncari.Location = New System.Drawing.Point(356, 19)
        Me.btncari.Name = "btncari"
        Me.btncari.Size = New System.Drawing.Size(68, 34)
        Me.btncari.TabIndex = 32
        Me.btncari.Text = "&Tampilkan"
        Me.btncari.UseVisualStyleBackColor = False
        '
        'txtnd
        '
        Me.txtnd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtnd.Location = New System.Drawing.Point(151, 23)
        Me.txtnd.Name = "txtnd"
        Me.txtnd.Size = New System.Drawing.Size(184, 26)
        Me.txtnd.TabIndex = 15
        '
        'lblnd
        '
        Me.lblnd.AutoSize = True
        Me.lblnd.Font = New System.Drawing.Font("Times New Roman", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblnd.Location = New System.Drawing.Point(39, 23)
        Me.lblnd.Name = "lblnd"
        Me.lblnd.Size = New System.Drawing.Size(91, 19)
        Me.lblnd.TabIndex = 31
        Me.lblnd.Text = "No. Delivery "
        '
        'Form_d_order
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(813, 456)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Form_d_order"
        Me.Text = "DELIVERY ORDER"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgtpb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnnew2 As System.Windows.Forms.Button
    Friend WithEvents btnsave2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txttotal As System.Windows.Forms.TextBox
    Friend WithEvents lbl_totalh As System.Windows.Forms.Label
    Friend WithEvents dptransaki As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtharga As System.Windows.Forms.TextBox
    Friend WithEvents lbl_harga As System.Windows.Forms.Label
    Friend WithEvents txtsat As System.Windows.Forms.TextBox
    Friend WithEvents lbl_sat As System.Windows.Forms.Label
    Friend WithEvents txtjumlah As System.Windows.Forms.TextBox
    Friend WithEvents lbl_jumlah As System.Windows.Forms.Label
    Friend WithEvents cmbnmbrg As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_nmbrg As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgtpb As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents txttotal2 As System.Windows.Forms.TextBox
    Friend WithEvents lbltotal2 As System.Windows.Forms.Label
    Friend WithEvents txtppn As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtst As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents checkboxcredit As System.Windows.Forms.CheckBox
    Friend WithEvents txtspb As System.Windows.Forms.TextBox
    Friend WithEvents lblsp As System.Windows.Forms.Label
    Friend WithEvents checkboxcash As System.Windows.Forms.CheckBox
    Friend WithEvents txtk As System.Windows.Forms.TextBox
    Friend WithEvents lblkredit As System.Windows.Forms.Label
    Friend WithEvents cmbdn As System.Windows.Forms.ComboBox
    Friend WithEvents lbl_delvno As System.Windows.Forms.Label
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents btnclear As System.Windows.Forms.Button
    Friend WithEvents btncari As System.Windows.Forms.Button
    Friend WithEvents txtnd As System.Windows.Forms.TextBox
    Friend WithEvents lblnd As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnprint As System.Windows.Forms.Button
End Class
