﻿Imports System.Data.OleDb
Public Class Form_sales_invoice
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Public id As Integer 'variabel public id data transaksi
    Dim db As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub cari_dtpb() 'FUNGSI CARI DATA TRANSAKSI PENGIRIMAN BARANG
        Try
            Dim dtpo As New AutoCompleteStringCollection()
            sql = "Select * From pengiriman_brg"
            Dim conn As New OleDbConnection(db)
            Dim cmd As New OleDbCommand(sql, conn)
            conn.Open()
            Dim dr As OleDbDataReader = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    dtpo.Add(dr("delivery_no"))
                End While
            Else
                MessageBox.Show("DATA TRANSAKSI PENGIRIMAN BARANG MASIH KOSONG !!!!")
            End If
            dr.Close()
            With txtnd
                .AutoCompleteCustomSource = dtpo
                .AutoCompleteSource = AutoCompleteSource.CustomSource
                .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub tampilkan_doj() 'FUNGSI TAMPILKAN PENCARIAN DATA ORDER JUAL

        sql = "select * from pengiriman_brg where delivery_no = '" & txtnd.Text & "' "
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then

            id = dtreader("id_p")
            txtcustomer.Text = dtreader("customer")
            txtnopo.Text = dtreader("no_po")
            dpdelv.Text = dtreader("delivery")
            txtnodelv.Text = dtreader("delivery_no")
            txtnopajak.Text = dtreader("no_faktur_p")
            txtnotes.Text = dtreader("notes")


            'disable button save
            btnsave.Enabled = False
            'enable button update dan delete aksi data transaksi
            btndlt.Enabled = True
            btnupdt.Enabled = True

        Else
            MsgBox("DATA TRANSAKSI TIDAK DITEMUKAN", MsgBoxStyle.Exclamation, "PERHATIAN")
            txtnd.Focus()
        End If
        conn.Close()
    End Sub
    Sub daftar()

        conn.Open()
        sql = "select * from pengiriman_brg"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgpengiriman.DataSource = ttabel
        conn.Close()

    End Sub
    Sub id_p() 'DATA ID DATA TRANSAKSI PENGIRIMAN BARANG

        sql = "select id_p from pengiriman_brg order by id_p desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_p") + 1
        Else
            id = 115
        End If
        conn.Close()
    End Sub
    Sub no_invoice() 'DATA NO INVOICE OTOMATIS ORDER JUAL
        sql = "select delivery_no from pengiriman_brg order by delivery_no desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            txtnodelv.Text = dtreader("delivery_no") + 1
        Else
            txtnodelv.Text = 1211
        End If
        conn.Close()
    End Sub
    Sub no_po() 'FUNGSI TAMPILKAN DATA COMBO BOX NO PO  ORDER JUAL

        sql = "select no_po from order_jual"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            cmbpo.Items.Add(dtreader("no_po"))
        End While
        conn.Close()
    End Sub
    Sub data_order() 'FUNGSI TAMPILKAN DATA ORDER JUAL SESUAI NO PO

        sql = "select * from order_jual where no_po = '" & cmbpo.Text & "'"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        While dtreader.Read()
            txtcustomer.Text = dtreader("customer")
            txtnopo.Text = dtreader("no_po")
            dpdelv.Text = dtreader("tgl_kirim")
        End While
        conn.Close()

        txtnopajak.Focus()

    End Sub
    Sub bersih() ' bersihkan isi form data customer
        cmbpo.Text = ""
        dpdelv.Text = ""
        txtcustomer.Text = ""
        txtnopo.Text = ""
        txtnodelv.Text = ""
        txtnopajak.Text = ""
        txtnotes.Text = "-"
        txtnd.Text = ""

        cmbpo.Focus()

    End Sub
    Sub simpan_pb() ' simpan data pengiriman barang
        'panggil koneksi database
        conn.Open()

        'variabel menampung data customer,no po, no faktur,no pajak,jenis pembayaran
        Dim c, po, dlvno, npajak, notes As String

        Dim dd As Date
        'variabel untuk delivery date

        id = id
        dd = dpdelv.Text
        c = txtcustomer.Text
        po = txtnopo.Text
        dlvno = txtnodelv.Text
        npajak = txtnopajak.Text
        notes = txtnotes.Text



            'PERINTAH SQL INPUT DATA KE DATABASE PENGIRIMAN_BRG
            sql = "insert into pengiriman_brg values('" & id & "','" & c & "','" & po & "','" & dd & "','" & dlvno & "','" & npajak & "','" & notes & "','-','0','0','0','0','0')"
            'EKSEKUSI PERINTAH SQL
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()

            conn.Close()

            MsgBox("Data Pengiriman Barang Berhasil Disimpan !!!", MsgBoxStyle.Information, "SUCCESS")

            bersih()
            id_p()
            cari_dtpb()
        
    End Sub
    Sub hapus() ' hapus data transaksi pengiriman barang
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data Transaksi Pengiriman Barang ini ? ", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then

            conn.Open()
            sql = "delete from pengiriman_brg where id_p ='" & id & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()

            MsgBox("Data Transaksi Pengiriman Barang Berhasil Dihapus !!!", MsgBoxStyle.Information, "SUCCESS")

            bersih()
            id_p()

            cari_dtpb()

            'enable button save
            btnsave.Enabled = True
            'disable button update dan delete aksi data transaksi
            btndlt.Enabled = False
            btnupdt.Enabled = False

        End If
    End Sub
    Sub ubah() 'ubah data barang transaksi pengiriman
        conn.Open()

        sql = "update pengiriman_brg set customer ='" & txtcustomer.Text & "',no_po ='" & txtnopo.Text & "',delivery='" & dpdelv.Text & "',delivery_no='" & txtnodelv.Text & "',no_faktur_p='" & txtnopajak.Text & "',notes='" & txtnotes.Text & "' where id_p ='" & id & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Transaksi Pengiriman Barang Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")

        cari_dtpb()
    End Sub
    Private Sub Form_transaksi_pb_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db() 'panggil koneksi database
        id_p() 'id otomatis transaksi
        no_po() ' combobox no po dari order jual
        cari_dtpb() ' untuk autocomplete pencarian no delivery - tampilkan data transaksi

        daftar()

    End Sub
    Private Sub btnclear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        bersih()

        'enable button save
        btnsave.Enabled = True
        'disable button update dan delete aksi data transaksi
        btndlt.Enabled = False
        btnupdt.Enabled = False

        id_p()

    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        If (txtnd.Text = "") Then
            MsgBox("NO DELIVERY MASING KOSONG", MsgBoxStyle.Exclamation, "PERHATIAN")
            txtnd.Focus()
        Else
            tampilkan_doj()
        End If
    End Sub

    Private Sub btnsave_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If (cmbpo.Text = "") Then
            MsgBox("NO PO BELUM ANDA PILIH", MsgBoxStyle.Exclamation)
            cmbpo.Focus()
        ElseIf (txtnopajak.Text = "") Then
            MsgBox("NO FAKTUR PAJAK MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnopajak.Focus()
        ElseIf (txtnotes.Text = "") Then
            MsgBox("CATATAN DATA MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnotes.Focus()
        Else
            simpan_pb()
            daftar()
        End If
    End Sub

    Private Sub btnupdt_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdt.Click
        If (txtnopajak.Text = "") Then
            MsgBox("NO FAKTUR PAJAK MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnopajak.Focus()
        ElseIf (txtnotes.Text = "") Then
            MsgBox("CATATAN DATA MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnotes.Focus()
        Else
            ubah()
            daftar()
        End If
    End Sub

    Private Sub btndlt_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndlt.Click
        hapus()
        daftar()
    End Sub

    Private Sub cmbpo_SelectedIndexChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmbpo.SelectedIndexChanged
        data_order()
        no_invoice()
    End Sub
    Private Sub btnnew_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        bersih()
        id_p()

        'enable button save
        btnsave.Enabled = True
        'disable button update dan delete aksi data transaksi
        btndlt.Enabled = False
        btnupdt.Enabled = False
    End Sub

    Private Sub btnprint_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnprint.Click
        'TAMPILKAN FORM INVOICE DELIVERY ORDER
        Form_invoice_order.CrystalReportViewer1.SelectionFormula = "{pengiriman_brg.delivery_no}='" & txtnodelv.Text & "' OR {pengiriman_brg.delivery_no}='" & txtnd.Text & "'"
        Form_invoice_order.CrystalReportViewer1.Refresh()
        Form_invoice_order.Show()

    End Sub

    Private Sub dgpengiriman_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgpengiriman.CellContentClick
        Dim baris = dgpengiriman.CurrentRow.Index()
        With dgpengiriman

            id = .Item(0, baris).Value
            txtcustomer.Text = .Item(1, baris).Value
            txtnopo.Text = .Item(2, baris).Value
            dpdelv.Text = .Item(3, baris).Value
            txtnodelv.Text = .Item(4, baris).Value
            txtnopajak.Text = .Item(5, baris).Value
            txtnotes.Text = .Item(6, baris).Value
           
            'enable button delete dan update
            btndlt.Enabled = True
            btnupdt.Enabled = True

            'disable button save
            btnsave.Enabled = False
        End With
    End Sub
End Class