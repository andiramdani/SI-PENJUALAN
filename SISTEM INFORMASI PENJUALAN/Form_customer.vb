﻿Imports System.Data.OleDb
Public Class Form_customer
    Public koneksi, sql As String
    Public conn As OleDb.OleDbConnection
    Public cmd As OleDb.OleDbCommand
    Public dtadapter As OleDb.OleDbDataAdapter
    Public dtreader As OleDb.OleDbDataReader
    Public ttabel As New DataTable
    Dim id As String
    Dim db As String = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
    Sub koneksi_db()
        koneksi = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" & Application.StartupPath + "\db_si_penjualan.mdb" & "'"
        conn = New OleDb.OleDbConnection(koneksi)
    End Sub
    Sub cari_dc() 'FUNGSI CARI DATA CUSTOMER
        Try
            Dim dtpo As New AutoCompleteStringCollection()
            sql = "Select * From customer"
            Dim conn As New OleDbConnection(db)
            Dim cmd As New OleDbCommand(sql, conn)
            conn.Open()
            Dim dr As OleDbDataReader = cmd.ExecuteReader()
            If dr.HasRows = True Then
                While dr.Read()
                    dtpo.Add(dr("customer"))
                End While
            Else
                MessageBox.Show("DATA CUSTOMER MASIH KOSONG !!!!")
            End If
            dr.Close()
            With txtnc
                .AutoCompleteCustomSource = dtpo
                .AutoCompleteSource = AutoCompleteSource.CustomSource
                .AutoCompleteMode = AutoCompleteMode.SuggestAppend
            End With
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Sub tampilkan_dc() 'FUNGSI TAMPILKAN PENCARIAN DATA CUSTOMER

        conn.Open()

        sql = "select * from customer where customer LIKE '" + txtnc.Text + "%'"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgcustomer.DataSource = ttabel
        conn.Close()

    End Sub
    Sub daftar()
        conn.Open()
        sql = "select * from customer"
        dtadapter = New OleDb.OleDbDataAdapter(sql, conn)
        ttabel.Clear()
        dtadapter.Fill(ttabel)
        dgcustomer.DataSource = ttabel
        conn.Close()
        txtnama.Focus()
    End Sub
    Sub bersih()

        id = ""
        txtnama.Text = ""
        txtalamat.Text = ""
        txtkota.Text = ""
        txttelepon.Text = ""
        txtfax.Text = ""
        txtcp.Text = ""
        txtnotes.Text = ""
        txtnc.Text = ""

        txtnama.Focus()
    End Sub
    Sub id_customer()
        sql = "select id_customer from customer order by id_customer desc"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_customer") + 1
        Else
            id = "10"
        End If
        conn.Close()
    End Sub
    Sub simpan()
        'panggil koneksi ke database
        conn.Open()

        'variabel menampung data nama ,alamat,kota,telepon,fax,cp,notes
        Dim nama, alamat, kota, telepon, fax, cp, nt As String

        nama = txtnama.Text
        alamat = txtalamat.Text
        kota = txtkota.Text
        telepon = txttelepon.Text
        fax = txtfax.Text
        cp = txtcp.Text
        nt = txtnotes.Text

        'PERINTAH SQL INPUT DATA KE DATABASE CUSTOMER
        sql = "insert into customer values('" & id & "','" & nama & "','" & alamat & "','" & kota & "','" & telepon & "','" & fax & "','" & cp & "','" & nt & "')"
        'EKSEKUSI PERINTAH SQL
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()

        MsgBox("Data Customer Berhasil Disimpan", MsgBoxStyle.Information, "SUCCESS")

    End Sub
    Sub hapus()
        Dim x As Integer
        x = MsgBox("Anda Yakin Akan Menghapus Data ini ?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Perhatian")
        If x = vbYes Then
            conn.Open()
            sql = "delete from customer where id_customer='" & id & "'"
            cmd = New OleDb.OleDbCommand(sql, conn)
            cmd.ExecuteNonQuery()
            conn.Close()

            MsgBox("Data User Berhasil Dihapus", MsgBoxStyle.Information, "SUCCESS")

            bersih()
        End If
    End Sub
    Sub ubah()
        conn.Open()
        sql = "update customer set customer ='" & txtnama.Text & "',alamat='" & txtalamat.Text & "',kota='" & txtkota.Text & "',telepon='" & txttelepon.Text & "',fax='" & txtfax.Text & "',cp='" & txtcp.Text & "',notes='" & txtnotes.Text & "' where id_customer ='" & id & "' "
        cmd = New OleDb.OleDbCommand(sql, conn)
        cmd.ExecuteNonQuery()
        conn.Close()
        MsgBox("Data Customer Berhasil Diperbarui !!!", MsgBoxStyle.Information, "SUCCESS")
    End Sub
    Sub cari()
        Dim x As String
        x = InputBox("Masukan id customer Yang Anda cari")
        sql = "select * from master_user where id_customer = '" & x & "'"
        conn.Open()
        cmd = New OleDb.OleDbCommand(sql, conn)
        dtreader = cmd.ExecuteReader
        If dtreader.Read Then
            id = dtreader("id_customer")
            txtnama.Text = dtreader("customer")
            txtalamat.Text = dtreader("alamat")
            txtkota.Text = dtreader("kota")
            txttelepon.Text = dtreader("telepon")
            txtfax.Text = dtreader("fax")
            txtcp.Text = dtreader("cp")
            txtnotes.Text = dtreader("notes")
        Else
            MsgBox("Data Customer Tidak Ditemukan", MsgBoxStyle.RetryCancel)
        End If
        conn.Close()
    End Sub
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        bersih()
        id_customer()

        'disable button delete dan update
        btndelete.Enabled = False
        btnupdate.Enabled = False

        'enable button save
        btnsave.Enabled = True

        daftar()

    End Sub

    Private Sub Form_customer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        koneksi_db()
        daftar()
        cari_dc()
        id_customer()

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If (txtnama.Text = "") Then
            MsgBox("NAMA MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtnama.Focus()
        ElseIf (txtalamat.Text = "") Then
            MsgBox("ALAMAT MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtalamat.Focus()
        ElseIf (txtkota.Text = "") Then
            MsgBox("KOTA MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtkota.Focus()
        ElseIf (txttelepon.Text = "") Then
            MsgBox("TELEPON MASIH KOSONG", MsgBoxStyle.Exclamation)
            txttelepon.Focus()
        ElseIf (txtfax.Text = "") Then
            MsgBox("FAX MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtfax.Focus()
        ElseIf (txtcp.Text = "") Then
            MsgBox("CONTACT PERSON MASIH KOSONG", MsgBoxStyle.Exclamation)
            txtfax.Focus()
        Else
            simpan()
            bersih()
            daftar()
            id_customer()
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        hapus()
        daftar()
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        ubah()
        daftar()
    End Sub

    Private Sub dgcustomer_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgcustomer.CellContentClick
        Dim baris = dgcustomer.CurrentRow.Index
        With dgcustomer

            id = .Item(0, baris).Value
            txtnama.Text = .Item(1, baris).Value
            txtalamat.Text = .Item(2, baris).Value
            txtkota.Text = .Item(3, baris).Value
            txttelepon.Text = .Item(4, baris).Value
            txtfax.Text = .Item(5, baris).Value
            txtcp.Text = .Item(6, baris).Value
            txtnotes.Text = .Item(7, baris).Value

            'enable button delete dan update
            btndelete.Enabled = True
            btnupdate.Enabled = True

            'disable button save
            btnsave.Enabled = False
        End With
    End Sub

    Private Sub btncari_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btncari.Click
        If (txtnc.Text = "") Then
            MsgBox("NAMA CUSTOMER BELUM ANDA ISI", MsgBoxStyle.Exclamation, "WARNING")
            txtnc.Focus()
        Else
            tampilkan_dc()
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclear.Click
        bersih()
        daftar()
    End Sub
End Class