﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_menu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_menu))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FILEToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ABOUTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EXITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INPUTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MASTERUSERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MASTERBARANGToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MASTERCUSTOMERToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INPUTToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ORDERJUALToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PENGIRIMANBRGToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LIHATDATAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TAMBAHTRANSAKSIToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FAKTURPAJAKToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DATAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CUSTOMERSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.REPORTSALESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BULANANToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CASHToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CREDITToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.TAHUNANToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CASHToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CREDITToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Turquoise
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FILEToolStripMenuItem, Me.INPUTToolStripMenuItem, Me.INPUTToolStripMenuItem1, Me.DATAToolStripMenuItem, Me.REPORTToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(655, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FILEToolStripMenuItem
        '
        Me.FILEToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ABOUTToolStripMenuItem, Me.EXITToolStripMenuItem})
        Me.FILEToolStripMenuItem.Name = "FILEToolStripMenuItem"
        Me.FILEToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
        Me.FILEToolStripMenuItem.Text = "FILE"
        '
        'ABOUTToolStripMenuItem
        '
        Me.ABOUTToolStripMenuItem.Name = "ABOUTToolStripMenuItem"
        Me.ABOUTToolStripMenuItem.Size = New System.Drawing.Size(113, 22)
        Me.ABOUTToolStripMenuItem.Text = "ABOUT"
        '
        'EXITToolStripMenuItem
        '
        Me.EXITToolStripMenuItem.Name = "EXITToolStripMenuItem"
        Me.EXITToolStripMenuItem.Size = New System.Drawing.Size(113, 22)
        Me.EXITToolStripMenuItem.Text = "EXIT"
        '
        'INPUTToolStripMenuItem
        '
        Me.INPUTToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MASTERUSERToolStripMenuItem, Me.MASTERBARANGToolStripMenuItem, Me.MASTERCUSTOMERToolStripMenuItem})
        Me.INPUTToolStripMenuItem.Name = "INPUTToolStripMenuItem"
        Me.INPUTToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.INPUTToolStripMenuItem.Text = "MASTER"
        '
        'MASTERUSERToolStripMenuItem
        '
        Me.MASTERUSERToolStripMenuItem.Name = "MASTERUSERToolStripMenuItem"
        Me.MASTERUSERToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.MASTERUSERToolStripMenuItem.Text = "MASTER USER"
        '
        'MASTERBARANGToolStripMenuItem
        '
        Me.MASTERBARANGToolStripMenuItem.Name = "MASTERBARANGToolStripMenuItem"
        Me.MASTERBARANGToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.MASTERBARANGToolStripMenuItem.Text = "MASTER BARANG"
        '
        'MASTERCUSTOMERToolStripMenuItem
        '
        Me.MASTERCUSTOMERToolStripMenuItem.Name = "MASTERCUSTOMERToolStripMenuItem"
        Me.MASTERCUSTOMERToolStripMenuItem.Size = New System.Drawing.Size(184, 22)
        Me.MASTERCUSTOMERToolStripMenuItem.Text = "MASTER CUSTOMER"
        '
        'INPUTToolStripMenuItem1
        '
        Me.INPUTToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ORDERJUALToolStripMenuItem, Me.PENGIRIMANBRGToolStripMenuItem})
        Me.INPUTToolStripMenuItem1.Name = "INPUTToolStripMenuItem1"
        Me.INPUTToolStripMenuItem1.Size = New System.Drawing.Size(53, 20)
        Me.INPUTToolStripMenuItem1.Text = "INPUT"
        '
        'ORDERJUALToolStripMenuItem
        '
        Me.ORDERJUALToolStripMenuItem.Name = "ORDERJUALToolStripMenuItem"
        Me.ORDERJUALToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.ORDERJUALToolStripMenuItem.Text = "TRANSAKSI PENJUALAN"
        '
        'PENGIRIMANBRGToolStripMenuItem
        '
        Me.PENGIRIMANBRGToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LIHATDATAToolStripMenuItem, Me.TAMBAHTRANSAKSIToolStripMenuItem, Me.FAKTURPAJAKToolStripMenuItem})
        Me.PENGIRIMANBRGToolStripMenuItem.Name = "PENGIRIMANBRGToolStripMenuItem"
        Me.PENGIRIMANBRGToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.PENGIRIMANBRGToolStripMenuItem.Text = "PENGIRIMAN BARANG"
        '
        'LIHATDATAToolStripMenuItem
        '
        Me.LIHATDATAToolStripMenuItem.Name = "LIHATDATAToolStripMenuItem"
        Me.LIHATDATAToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.LIHATDATAToolStripMenuItem.Text = "LIHAT DATA"
        '
        'TAMBAHTRANSAKSIToolStripMenuItem
        '
        Me.TAMBAHTRANSAKSIToolStripMenuItem.Name = "TAMBAHTRANSAKSIToolStripMenuItem"
        Me.TAMBAHTRANSAKSIToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.TAMBAHTRANSAKSIToolStripMenuItem.Text = "SURAT JALAN"
        '
        'FAKTURPAJAKToolStripMenuItem
        '
        Me.FAKTURPAJAKToolStripMenuItem.Name = "FAKTURPAJAKToolStripMenuItem"
        Me.FAKTURPAJAKToolStripMenuItem.Size = New System.Drawing.Size(154, 22)
        Me.FAKTURPAJAKToolStripMenuItem.Text = "FAKTUR PAJAK"
        '
        'DATAToolStripMenuItem
        '
        Me.DATAToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CUSTOMERSToolStripMenuItem})
        Me.DATAToolStripMenuItem.Name = "DATAToolStripMenuItem"
        Me.DATAToolStripMenuItem.Size = New System.Drawing.Size(50, 20)
        Me.DATAToolStripMenuItem.Text = "DATA"
        '
        'CUSTOMERSToolStripMenuItem
        '
        Me.CUSTOMERSToolStripMenuItem.Name = "CUSTOMERSToolStripMenuItem"
        Me.CUSTOMERSToolStripMenuItem.Size = New System.Drawing.Size(142, 22)
        Me.CUSTOMERSToolStripMenuItem.Text = "CUSTOMERS"
        '
        'REPORTToolStripMenuItem
        '
        Me.REPORTToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.REPORTSALESToolStripMenuItem})
        Me.REPORTToolStripMenuItem.Name = "REPORTToolStripMenuItem"
        Me.REPORTToolStripMenuItem.Size = New System.Drawing.Size(62, 20)
        Me.REPORTToolStripMenuItem.Text = "REPORT"
        '
        'REPORTSALESToolStripMenuItem
        '
        Me.REPORTSALESToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BULANANToolStripMenuItem, Me.TAHUNANToolStripMenuItem})
        Me.REPORTSALESToolStripMenuItem.Name = "REPORTSALESToolStripMenuItem"
        Me.REPORTSALESToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.REPORTSALESToolStripMenuItem.Text = "PENJUALAN BARANG"
        '
        'BULANANToolStripMenuItem
        '
        Me.BULANANToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CASHToolStripMenuItem1, Me.CREDITToolStripMenuItem1})
        Me.BULANANToolStripMenuItem.Name = "BULANANToolStripMenuItem"
        Me.BULANANToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BULANANToolStripMenuItem.Text = "BULANAN"
        '
        'CASHToolStripMenuItem1
        '
        Me.CASHToolStripMenuItem1.Name = "CASHToolStripMenuItem1"
        Me.CASHToolStripMenuItem1.Size = New System.Drawing.Size(113, 22)
        Me.CASHToolStripMenuItem1.Text = "CASH"
        '
        'CREDITToolStripMenuItem1
        '
        Me.CREDITToolStripMenuItem1.Name = "CREDITToolStripMenuItem1"
        Me.CREDITToolStripMenuItem1.Size = New System.Drawing.Size(113, 22)
        Me.CREDITToolStripMenuItem1.Text = "CREDIT"
        '
        'TAHUNANToolStripMenuItem
        '
        Me.TAHUNANToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CASHToolStripMenuItem, Me.CREDITToolStripMenuItem})
        Me.TAHUNANToolStripMenuItem.Name = "TAHUNANToolStripMenuItem"
        Me.TAHUNANToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.TAHUNANToolStripMenuItem.Text = "TAHUNAN"
        '
        'CASHToolStripMenuItem
        '
        Me.CASHToolStripMenuItem.Name = "CASHToolStripMenuItem"
        Me.CASHToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CASHToolStripMenuItem.Text = "CASH"
        '
        'CREDITToolStripMenuItem
        '
        Me.CREDITToolStripMenuItem.Name = "CREDITToolStripMenuItem"
        Me.CREDITToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.CREDITToolStripMenuItem.Text = "CREDIT"
        '
        'Form_menu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(655, 364)
        Me.Controls.Add(Me.MenuStrip1)
        Me.DoubleBuffered = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "Form_menu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MENU UTAMA - SI - PENJUALAN"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FILEToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ABOUTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EXITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents INPUTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MASTERUSERToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MASTERBARANGToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents INPUTToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ORDERJUALToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PENGIRIMANBRGToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents REPORTSALESToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MASTERCUSTOMERToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BULANANToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TAHUNANToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LIHATDATAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TAMBAHTRANSAKSIToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DATAToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CUSTOMERSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FAKTURPAJAKToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CASHToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CREDITToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CASHToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CREDITToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
End Class
